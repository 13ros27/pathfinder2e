use bevy::prelude::*;

use crate::simulation::{SimStart, Simulation};

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Queued; // TODO: Reactions will need this completely reworked

fn update_queue(mut commands: Commands, queued: Query<Entity, With<Queued>>) {
    for entity in &queued {
        commands.entity(entity).remove::<Queued>();
    }
}

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ShortLived;

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct QueuePlugin;

impl Plugin for QueuePlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Simulation, update_queue.before(SimStart));
    }
}

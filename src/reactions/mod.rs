mod goblin_scuttle;

use bevy::{
    ecs::component::{ComponentInfo, TableStorage},
    prelude::*,
};

use crate::{
    graphics::input::AwaitingInput,
    queue::ShortLived,
    simulation::{SimReact, SimStart},
};

pub mod prelude {
    pub use super::goblin_scuttle::GoblinScuttle;
}

pub use goblin_scuttle::GoblinScuttle;

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Reaction;

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ApplyReaction;

impl Component for ApplyReaction {
    type Storage = TableStorage;
    fn init_component_info(info: &mut ComponentInfo) {
        info.on_add(|mut w, e, _| {
            w.commands().entity(e).insert((AwaitingInput, ShortLived));
        });
    }
}

pub struct ReactionPlugin;

impl Plugin for ReactionPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(SimStart, GoblinScuttle::apply)
            .add_systems(SimReact, GoblinScuttle::react);
    }
}

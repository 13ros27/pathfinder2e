use bevy::prelude::*;

use crate::{
    actions::Step,
    creature_info::{races::Goblin, Position},
    creatures::Creature,
    movement::{MovementPlan, MovementReaction, NoTrigger},
    names::Named,
    queue::Queued,
    reactions::{ApplyReaction, Reaction},
    relations::prelude::*,
    simulation::{SimulationInput, TriggerAction},
    Action,
};

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GoblinScuttle;

impl Named for GoblinScuttle {
    const NAME: &'static str = "Goblin Scuttle";
}

impl GoblinScuttle {
    pub fn react(
        mut commands: Commands,
        mut movement: Query<
            (
                Entity,
                &MovementPlan,
                &Relation<Creature>,
                Option<&mut Relations<MovementReaction>>,
            ),
            Without<NoTrigger>,
        >,
        is_goblin: Query<Has<Goblin>, With<Creature>>,
        scuttle_creatures: Query<(Entity, &Relation<Creature>), With<GoblinScuttle>>,
        scuttle_positions: Query<&Position, With<Creature>>,
    ) {
        for (ent, plan, &creature, reactions) in &mut movement {
            if is_goblin.get(*creature).unwrap() {
                let mut new_reactions = Vec::new();
                for (scuttle_reaction, &reacting_creature) in &scuttle_creatures {
                    // Goblins can't scuttle as a reaction to themself moving
                    if reacting_creature != creature {
                        let position = scuttle_positions.get(*reacting_creature).unwrap();
                        if plan.end().adjacent(position) {
                            new_reactions.push(
                                commands
                                    .spawn((MovementReaction, reacting_creature))
                                    .insert_relation::<Reaction>(scuttle_reaction)
                                    .id(),
                            )
                        }
                    }
                }

                if !new_reactions.is_empty() {
                    if let Some(mut reactions) = reactions {
                        reactions.append_unchecked(new_reactions);
                    } else {
                        commands
                            .entity(ent)
                            .insert_relations::<MovementReaction>(new_reactions);
                    }
                }
            }
        }
    }

    pub fn apply(
        mut commands: Commands,
        trigger: Res<SimulationInput>,
        reaction_info: Query<&Relation<ApplyReaction>, With<TriggerAction>>,
        creatures: Query<
            (&Relation<Creature>, &Relation<Reaction>),
            (With<ApplyReaction>, Without<Queued>),
        >,
        creature_actions: Query<&Relations<Action>, With<Creature>>,
        scuttle_reactions: Query<(), With<GoblinScuttle>>,
        step_actions: Query<(), With<Step>>,
    ) {
        if let Some(input) = **trigger
            && let Ok(&apply_reaction) = reaction_info.get(*input)
            && let Ok((&creature, &reaction)) = creatures.get(*apply_reaction)
            && scuttle_reactions.contains(*reaction)
        {
            let actions = creature_actions.get(*creature).unwrap();
            for action in &**actions {
                if step_actions.contains(**action) {
                    commands.spawn((TriggerAction, Queued, *action));
                    break;
                }
            }
        }
    }
}

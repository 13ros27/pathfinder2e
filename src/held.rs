use bevy::{
    ecs::{component::TableStorage, system::EntityCommand},
    prelude::*,
};

use crate::{creatures::Creature, relations::prelude::*, weapons::Item, Action};

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Hands {
    One,
    Two,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Held {
    Left,
    Right,
    Both,
}

impl Component for Held {
    type Storage = TableStorage;
    fn init_component_info(info: &mut bevy::ecs::component::ComponentInfo) {
        info.on_add(|mut world, entity, _| {
            world
                .commands()
                .entity(entity)
                .add(LinkActions::<HandsFilter>);
        });
        info.on_remove(|mut world, entity, _| {
            let item_actions = world.get::<Relations<Action>>(entity).unwrap().clone();
            let creature = **world.get::<Relation<Creature>>(entity).unwrap();
            let mut creature_actions = world.get_mut::<Relations<Action>>(creature).unwrap();
            for action in item_actions.iter() {
                creature_actions.remove(action);
            }
        });
    }
}

// A weapon which is a part of the creature, for example Jaws
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Innate;

impl Component for Innate {
    type Storage = TableStorage;
    fn init_component_info(info: &mut bevy::ecs::component::ComponentInfo) {
        info.on_add(|mut world, entity, _| {
            // It has to be in a command because other things have to be added first (like Relation<Creature>)
            world.commands().entity(entity).add(LinkActions::<()>);
        });
    }
}

#[ghost::phantom]
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct LinkActions<F: ActionFilter>;

impl<F: ActionFilter + Send + 'static> EntityCommand for LinkActions<F> {
    fn apply(self, id: Entity, world: &mut World) {
        let creature = **world.get::<Relation<Creature>>(id).unwrap();
        let added_actions: Vec<_> = world
            .get::<Relations<Action>>(id)
            .unwrap()
            .iter()
            .filter(|&&action| F::filter(world, *action))
            .map(|action| **action)
            .collect();

        for action in &added_actions {
            world
                .entity_mut(*action)
                .insert(Relation::<Creature>::new_unchecked(creature));
        }

        if !added_actions.is_empty() {
            if let Some(mut related) = world.get_mut::<Relations<Action>>(creature) {
                related.extend(Relations::new_unchecked(added_actions));
            } else {
                world
                    .entity_mut(creature)
                    .insert(Relations::<Action>::new_unchecked(added_actions));
            }
        }
    }
}

trait ActionFilter {
    fn filter(_world: &World, _action: Entity) -> bool {
        true
    }
}

impl ActionFilter for () {}

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct HandsFilter;

impl ActionFilter for HandsFilter {
    fn filter(world: &World, action: Entity) -> bool {
        let item = **world.get::<Relation<Item>>(action).unwrap();
        let hands = *world.get::<Hands>(item).unwrap();
        let held = *world.get::<Held>(item).unwrap();
        match hands {
            Hands::One => true,
            Hands::Two => held == Held::Both,
        }
    }
}

use bevy::{
    ecs::component::{ComponentInfo, TableStorage},
    prelude::*,
};

use crate::{
    creature_info::Position,
    creatures::Creature,
    graphics::events::GraphicsEvents,
    queue::{Queued, ShortLived},
    reactions::ApplyReaction,
    relations::prelude::*,
    simulation::SimFinish,
};

/// Marks that a particular action doesn't trigger reactions (e.g. Step)
#[derive(Component, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct NoTrigger;

#[derive(Component, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct MovementReaction;

#[derive(Clone, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct MovementPlan(pub Vec<Position>);

impl Component for MovementPlan {
    type Storage = TableStorage;
    fn init_component_info(info: &mut ComponentInfo) {
        info.on_add(|mut w, e, _| {
            w.commands().entity(e).insert(ShortLived);
        });
    }
}

impl MovementPlan {
    pub fn end(&self) -> Position {
        self[self.len() - 1]
    }

    fn apply(
        mut commands: Commands,
        mut graphics_events: ResMut<GraphicsEvents>,
        query: Query<(
            Entity,
            &MovementPlan,
            &Relation<Creature>,
            Option<&Relations<MovementReaction>>,
        )>,
        mut positions: Query<&mut Position, With<Creature>>,
    ) {
        for (ent, plan, &creature, reactions) in &query {
            let mut self_pos = positions.get_mut(*creature).unwrap();
            *self_pos = plan.end();
            bevy::utils::tracing::info!("Moved from {} to {}", plan[0], *self_pos);

            graphics_events.send_movement(&mut commands, creature, plan.clone());

            // TODO: Add a plan position to some MovementReactions if they want to go part way
            // TODO: Add support for multiple reactions on the same movement (needs a proper queue)
            if let Some(reactions) = reactions {
                if reactions.len() > 1 {
                    bevy::utils::tracing::error!("Don't yet support multiple reactions on the same movement, taking the first");
                }

                let reaction = commands
                    .entity(**reactions.iter().next().unwrap())
                    .remove::<MovementReaction>()
                    .insert((ApplyReaction, Queued))
                    .id();

                // TODO: remove_relations custom command?
                commands.entity(ent).remove::<Relations<MovementReaction>>();

                graphics_events.send_reaction(&mut commands, KindEntity::new(reaction));
            }
        }
    }
}

pub struct MovementPlugin;

impl Plugin for MovementPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(SimFinish, MovementPlan::apply);
    }
}

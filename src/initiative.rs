use bevy::prelude::*;
use std::collections::VecDeque;

use crate::{creatures::Creature, relations::prelude::*};

#[derive(Resource, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Initiative(VecDeque<KindEntity<Creature>>);

impl Initiative {
    /// Create an empty initiative queue
    pub fn empty() -> Self {
        Self(VecDeque::new())
    }

    /// Add the given creatures to the end of the initiative order
    pub fn extend(&mut self, creatures: Vec<Entity>) {
        self.0.extend(creatures.into_iter().map(KindEntity::new))
    }

    /// Get the current creature in initiative order
    pub fn current(&self) -> KindEntity<Creature> {
        self.0[0]
    }

    /// Progress the initiative order to the next creature
    pub fn progress(&mut self) {
        if let Some(popped) = self.0.pop_front() {
            self.0.push_back(popped);
        }
    }

    /// Remove the given creature from the initiative order
    pub fn remove(&mut self, creature: KindEntity<Creature>) -> Result<(), ()> {
        let index = self.0.iter().position(|c| c == &creature).ok_or(())?;
        self.0.remove(index);
        Ok(())
    }

    /// Move the given creature to just before the current place in the iniative
    pub fn move_to_end(&mut self, creature: KindEntity<Creature>) -> Result<(), ()> {
        let index = self.0.iter().position(|c| c == &creature).ok_or(())?;
        self.0.remove(index);
        self.0.push_back(creature);
        Ok(())
    }
}

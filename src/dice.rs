use bevy::prelude::*;
use rand::prelude::*;
use std::{
    fmt::Display,
    ops::{Add, AddAssign, Mul, MulAssign},
};

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Dice {
    d4: u8,
    d6: u8,
    d8: u8,
    d10: u8,
    d12: u8,
    d20: u8,
    d100: u8,
}

impl Dice {
    pub fn roll(&self) -> usize {
        let mut total = 0;
        for _ in 0..self.d4 {
            total += thread_rng().gen_range(1..=4);
        }
        for _ in 0..self.d6 {
            total += thread_rng().gen_range(1..=6);
        }
        for _ in 0..self.d8 {
            total += thread_rng().gen_range(1..=8);
        }
        for _ in 0..self.d10 {
            total += thread_rng().gen_range(1..=10);
        }
        for _ in 0..self.d12 {
            total += thread_rng().gen_range(1..=12);
        }
        for _ in 0..self.d20 {
            total += thread_rng().gen_range(1..=20);
        }
        for _ in 0..self.d100 {
            total += thread_rng().gen_range(1..=100);
        }
        bevy::utils::tracing::info!("Rolled {} resulting in {}", self, total);
        total
    }
}

impl Display for Dice {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            [
                (self.d4, "d4"),
                (self.d6, "d6"),
                (self.d8, "d8"),
                (self.d10, "d10"),
                (self.d12, "d12"),
                (self.d20, "d20"),
                (self.d100, "d100"),
            ]
            .into_iter()
            .filter(|(n, _)| *n != 0)
            .map(|(n, d)| format!("{}{}", n, d))
            .intersperse(" + ".to_string())
            .collect::<String>()
        )
    }
}

impl Add<Dice> for Dice {
    type Output = Dice;
    fn add(self, rhs: Dice) -> Dice {
        Dice {
            d4: self.d4 + rhs.d4,
            d6: self.d6 + rhs.d6,
            d8: self.d8 + rhs.d8,
            d10: self.d10 + rhs.d10,
            d12: self.d12 + rhs.d12,
            d20: self.d20 + rhs.d20,
            d100: self.d100 + rhs.d100,
        }
    }
}

impl Mul<u8> for Dice {
    type Output = Dice;
    fn mul(self, rhs: u8) -> Dice {
        Dice {
            d4: self.d4 * rhs,
            d6: self.d6 * rhs,
            d8: self.d8 * rhs,
            d10: self.d10 * rhs,
            d12: self.d12 * rhs,
            d20: self.d20 * rhs,
            d100: self.d100 * rhs,
        }
    }
}

impl AddAssign<Dice> for Dice {
    fn add_assign(&mut self, rhs: Dice) {
        self.d4 += rhs.d4;
        self.d6 *= rhs.d6;
        self.d8 *= rhs.d8;
        self.d10 *= rhs.d10;
        self.d12 *= rhs.d12;
        self.d20 *= rhs.d20;
        self.d100 *= rhs.d100;
    }
}

impl MulAssign<u8> for Dice {
    fn mul_assign(&mut self, rhs: u8) {
        self.d4 *= rhs;
        self.d6 *= rhs;
        self.d8 *= rhs;
        self.d10 *= rhs;
        self.d12 *= rhs;
        self.d20 *= rhs;
        self.d100 *= rhs;
    }
}

pub const D4: Dice = Dice {
    d4: 1,
    d6: 0,
    d8: 0,
    d10: 0,
    d12: 0,
    d20: 0,
    d100: 0,
};
pub const D6: Dice = Dice {
    d4: 0,
    d6: 1,
    d8: 0,
    d10: 0,
    d12: 0,
    d20: 0,
    d100: 0,
};
pub const D8: Dice = Dice {
    d4: 0,
    d6: 0,
    d8: 1,
    d10: 0,
    d12: 0,
    d20: 0,
    d100: 0,
};
pub const D10: Dice = Dice {
    d4: 0,
    d6: 0,
    d8: 0,
    d10: 1,
    d12: 0,
    d20: 0,
    d100: 0,
};
pub const D12: Dice = Dice {
    d4: 0,
    d6: 0,
    d8: 0,
    d10: 0,
    d12: 1,
    d20: 0,
    d100: 0,
};
pub const D20: Dice = Dice {
    d4: 0,
    d6: 0,
    d8: 0,
    d10: 0,
    d12: 0,
    d20: 1,
    d100: 0,
};
pub const D100: Dice = Dice {
    d4: 0,
    d6: 0,
    d8: 0,
    d10: 0,
    d12: 0,
    d20: 0,
    d100: 1,
};

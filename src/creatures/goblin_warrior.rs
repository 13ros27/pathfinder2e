use bevy::prelude::*;

use crate::creatures::builder::prelude::*;

pub struct GoblinWarrior;

impl SpawnCreature for GoblinWarrior {
    fn spawn(self, world: &mut World) -> EntityWorldMut {
        BuildCreature::new(world)
            .name("Goblin Warrior")
            .level(-1)
            .perception(2)
            .stats(0, 3, 1, 0, -1, 1)
            .ac(16)
            .saves(5, 7, 3)
            .hp(6)
            .speed(25)
            .with_default::<(Goblin, Humanoid)>()
            .add_action::<Step>(|a| a.cost(1))
            .add_action::<Stride>(|a| a.cost(1))
            .add_item::<Dogslicer>(|i| {
                i.add_action::<Strike>(|a| a.cost(1).fixed_bonus(5))
                    .held_left()
            })
            .add_item::<Shortbow>(|i| i.add_action::<Strike>(|a| a.cost(1).fixed_bonus(5)))
            .add_reaction::<GoblinScuttle>()
            .spawn()
    }
}

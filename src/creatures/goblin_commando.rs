use bevy::prelude::*;

use crate::creatures::builder::prelude::*;

pub struct GoblinCommando;

impl SpawnCreature for GoblinCommando {
    fn spawn(self, world: &mut World) -> EntityWorldMut {
        BuildCreature::new(world)
            .name("Goblin Commando")
            .level(1)
            .perception(5)
            .stats(3, 3, 2, -1, 0, 2)
            .ac(17)
            .saves(7, 8, 5)
            .hp(18)
            .speed(25)
            .death_threshold(3) // TODO: For testing purposes
            .with_default::<(Goblin, Humanoid)>()
            .add_action::<Step>(|a| a.cost(1))
            .add_action::<Stride>(|a| a.cost(1))
            .add_item::<Horsechopper>(|i| {
                i.add_action::<Strike>(|a| a.cost(1).fixed_bonus(5))
                    .held_both()
            })
            .add_item::<Shortbow>(|i| i.add_action::<Strike>(|a| a.cost(1).fixed_bonus(5)))
            .add_reaction::<GoblinScuttle>()
            .spawn()
    }
}

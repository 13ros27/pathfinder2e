use bevy::prelude::*;
use std::marker::PhantomData;

use crate::{
    actions::{
        ActionCost, FixedAttackBonus, MultiAttackPenalty, Strike, ValidActions, ValidatedAction,
    },
    conditions::Condition,
    creature_info::{stats::StatsBundle, Saves},
    creatures::Creature,
    dying::DeathThreshold,
    graphics::input::InputValidation,
    held::Held,
    names::{FixedName, Named},
    reactions::Reaction,
    relations::prelude::*,
    weapons::Item,
    Ac, Action, Hp, Level, MaxHP, Perception, Speed,
};

pub mod prelude {
    pub use super::BuildCreature;
    pub use crate::{
        actions::all::*, creature_info::races::*, creatures::SpawnCreature, reactions::prelude::*,
        weapons::all::*,
    };
}

pub struct BuildCreature<'a, B: Bundle> {
    world: &'a mut World,
    name: Option<String>,
    level: Option<isize>,
    perception: Option<isize>,
    stats: Option<StatsBundle>,
    ac: Option<usize>,
    saves: Option<Saves>,
    hp: Option<usize>,
    speed: Option<usize>,
    with: B,
    items: Option<Relations<Item>>,
    actions: Option<Relations<Action>>,
    reactions: Option<Relations<Reaction>>,
    death_threshold: usize,
}

impl<'a> BuildCreature<'a, ()> {
    pub fn new(world: &'a mut World) -> Self {
        Self {
            world,
            name: None,
            level: None,
            perception: None,
            stats: None,
            ac: None,
            saves: None,
            hp: None,
            speed: None,
            with: (),
            items: None,
            actions: None,
            reactions: None,
            death_threshold: 0,
        }
    }
}

impl<'a, B: Bundle> BuildCreature<'a, B> {
    pub fn spawn(self) -> EntityWorldMut<'a> {
        let creature = self
            .world
            .spawn((
                Creature,
                MultiAttackPenalty::default(),
                FixedName(self.name.expect("A creature must be given a name")),
                Level(self.level.expect("A creature must have a level")),
                Perception(self.perception.expect("A creature must have perception")),
                self.stats.expect("A creature must have stats"),
                Ac(self.ac.expect("A creature must have an AC")),
                self.saves.expect("A creature must have saves"),
                MaxHP(self.hp.expect("A creature must have a maximum hp")),
                Hp(self.hp.unwrap()),
                Speed(self.speed.expect("A creature must have a speed")),
                Relations::<Condition>::empty(),
                DeathThreshold(self.death_threshold),
                self.with,
            ))
            .id();

        if let Some(items) = self.items {
            for item in &*items {
                self.world
                    .entity_mut(**item)
                    .insert(Relation::<Creature>::new_unchecked(creature));
            }

            self.world.entity_mut(creature).insert(items);
        }

        if let Some(actions) = self.actions {
            for action in &*actions {
                self.world
                    .entity_mut(**action)
                    .insert(Relation::<Creature>::new_unchecked(creature));
            }

            self.world.entity_mut(creature).insert(actions);
        }

        if let Some(reactions) = self.reactions {
            for reaction in &*reactions {
                self.world
                    .entity_mut(**reaction)
                    .insert(Relation::<Creature>::new_unchecked(creature));
            }

            self.world.entity_mut(creature).insert(reactions);
        }

        self.world.entity_mut(creature)
    }

    pub fn name(mut self, name: impl Into<String>) -> Self {
        self.name = Some(name.into());
        self
    }

    pub fn level(mut self, level: isize) -> Self {
        self.level = Some(level);
        self
    }

    pub fn perception(mut self, perception: isize) -> Self {
        self.perception = Some(perception);
        self
    }

    pub fn stats(
        mut self,
        str: isize,
        dex: isize,
        con: isize,
        int: isize,
        wis: isize,
        cha: isize,
    ) -> Self {
        self.stats = Some(StatsBundle::new(str, dex, con, int, wis, cha));
        self
    }

    pub fn ac(mut self, ac: usize) -> Self {
        self.ac = Some(ac);
        self
    }

    pub fn saves(mut self, fort: isize, refl: isize, will: isize) -> Self {
        self.saves = Some(Saves::new(fort, refl, will));
        self
    }

    pub fn hp(mut self, max_hp: usize) -> Self {
        self.hp = Some(max_hp);
        self
    }

    pub fn speed(mut self, speed: usize) -> Self {
        self.speed = Some(speed);
        self
    }

    pub fn death_threshold(mut self, threshold: usize) -> Self {
        self.death_threshold = threshold;
        self
    }

    pub fn with<T: Bundle>(self, bundle: T) -> BuildCreature<'a, (B, T)> {
        BuildCreature::<(B, T)> {
            with: (self.with, bundle),
            ..self // #[feature(type_changing_struct_update)] is magic
        }
    }

    pub fn with_default<T: Bundle + Default>(self) -> BuildCreature<'a, (B, T)> {
        self.with::<T>(default())
    }

    pub fn add_item<I: Bundle + Named>(
        mut self,
        f: impl Fn(Box<BuildItem<'_, I>>) -> Box<dyn Spawnable<Item, I> + '_>,
    ) -> Self {
        let item = f(BuildItem::new(self.world)).spawn();

        if let Some(ref mut items) = self.items {
            items.add(Relation::new_unchecked(*item));
        } else {
            self.items = Some(Relations::new_unchecked(vec![*item]));
        }
        self
    }

    pub fn add_action<A: Component + Default + Named + ValidatedAction>(
        mut self,
        f: impl Fn(Box<BuildAction<'_, A>>) -> Box<dyn Spawnable<Action, A> + '_>,
    ) -> Self {
        let action = BuildAction::add(self.world, &mut self.actions, f);
        self.world.entity_mut(*action).insert(A::get_name());
        self
    }

    pub fn add_reaction<T: Component + Default + Named>(mut self) -> Self {
        let reaction = self
            .world
            .spawn((T::default(), Reaction, T::get_name()))
            .id();

        if let Some(ref mut reactions) = self.reactions {
            reactions.add(Relation::new_unchecked(reaction));
        } else {
            self.reactions = Some(Relations::new_unchecked(vec![reaction]));
        }
        self
    }
}

pub trait Spawnable<T: Component + Default, B> {
    fn spawn(self: Box<Self>) -> KindEntity<T>;
}

pub trait SpawnWith<T: Component + Default, B: Bundle> {
    fn spawn_with(self: Box<Self>, bundle: B) -> Box<KindEntity<T>>;
}

impl<T: Component + Default, B: Bundle> SpawnWith<T, B> for KindEntity<T> {
    fn spawn_with(self: Box<KindEntity<T>>, _: B) -> Box<KindEntity<T>> {
        self
    }
}

impl<T, B, S> Spawnable<T, B> for S
where
    T: Component + Default,
    B: Bundle + Default,
    S: SpawnWith<T, B>,
{
    fn spawn(self: Box<Self>) -> KindEntity<T> {
        *self.spawn_with(B::default())
    }
}

pub struct BuildItem<'a, I: Bundle> {
    world: &'a mut World,
    actions: Option<Relations<Action>>,
    hands: Option<Held>,
    _marker: PhantomData<I>,
}

impl<'a, I: Bundle + Named> SpawnWith<Item, I> for BuildItem<'a, I> {
    fn spawn_with(self: Box<Self>, bundle: I) -> Box<KindEntity<Item>> {
        let item = self.world.spawn((Item, bundle, I::get_name())).id();

        if let Some(hands) = self.hands {
            self.world.entity_mut(item).insert(hands);
        }

        if let Some(actions) = self.actions {
            for action in &*actions {
                self.world
                    .entity_mut(**action)
                    .insert(Relation::<Item>::new_unchecked(item));
            }

            self.world.entity_mut(item).insert(actions);
        }

        Box::new(KindEntity::new(item))
    }
}

impl<'a, I: Bundle + Named> BuildItem<'a, I> {
    fn new(world: &'a mut World) -> Box<Self> {
        Box::new(Self {
            world,
            actions: None,
            hands: None,
            _marker: default(),
        })
    }

    pub fn add_action<A>(
        mut self: Box<Self>,
        f: impl Fn(Box<BuildAction<'_, A>>) -> Box<dyn Spawnable<Action, A> + '_>,
    ) -> Box<Self>
    where
        A: Component + Default + Named + ValidatedAction,
    {
        let action = BuildAction::add(self.world, &mut self.actions, f);
        self.world
            .entity_mut(*action)
            .insert(FixedName(format!("{} ({})", A::NAME, I::NAME)));
        self
    }

    pub fn held(mut self: Box<Self>, hands: Held) -> Box<Self> {
        self.hands = Some(hands);
        self
    }

    pub fn held_left(self: Box<Self>) -> Box<Self> {
        self.held(Held::Left)
    }

    pub fn held_right(self: Box<Self>) -> Box<Self> {
        self.held(Held::Right)
    }

    pub fn held_both(self: Box<Self>) -> Box<Self> {
        self.held(Held::Both)
    }
}

pub struct BuildAction<'a, A: ValidatedAction> {
    world: &'a mut World,
    cost: Option<usize>,
    validation: InputValidation<A::Input>,
}

impl<'a, A: Component + ValidatedAction> SpawnWith<Action, A> for BuildAction<'a, A> {
    fn spawn_with(self: Box<Self>, bundle: A) -> Box<KindEntity<Action>> {
        let action = self
            .world
            .spawn((
                Action,
                bundle,
                self.validation,
                ActionCost(self.cost.expect("An action must have a cost")),
            ))
            .id();

        Box::new(KindEntity::new(action))
    }
}

impl<'a, A: Component + Default + ValidatedAction> BuildAction<'a, A> {
    fn add(
        world: &'a mut World,
        actions: &mut Option<Relations<Action>>,
        f: impl Fn(Box<BuildAction<'_, A>>) -> Box<dyn Spawnable<Action, A> + '_>,
    ) -> KindEntity<Action> {
        let validation = InputValidation {
            system: world
                .get_resource::<ValidActions>()
                .unwrap()
                .get::<A>(world),
        };
        let action_builder = BuildAction {
            world,
            cost: None,
            validation,
        };
        let action = f(Box::new(action_builder)).spawn();

        if let Some(ref mut actions) = actions {
            actions.add(Relation::new_unchecked(*action));
        } else {
            *actions = Some(Relations::new_unchecked(vec![*action]));
        }

        action
    }

    pub fn cost(mut self: Box<Self>, cost: usize) -> Box<Self> {
        self.cost = Some(cost);
        self
    }
}

impl<'a> BuildAction<'a, Strike> {
    pub fn fixed_bonus(self, bonus: isize) -> Box<BuildStrike<'a>> {
        BuildStrike::new(self).fixed_bonus(bonus)
    }
}

pub struct BuildStrike<'a> {
    action: BuildAction<'a, Strike>,
    fixed_bonus: Option<isize>,
}

impl<'a> SpawnWith<Action, Strike> for BuildStrike<'a> {
    fn spawn_with(self: Box<Self>, bundle: Strike) -> Box<KindEntity<Action>> {
        let mut action = self.action.world.spawn((
            Action,
            bundle,
            self.action.validation,
            ActionCost(self.action.cost.expect("An action must have a cost")),
        ));

        if let Some(bonus) = self.fixed_bonus {
            action.insert(FixedAttackBonus(bonus));
        }

        Box::new(KindEntity::new(action.id()))
    }
}

impl<'a> BuildStrike<'a> {
    fn new(action: BuildAction<'a, Strike>) -> Box<Self> {
        Box::new(Self {
            action,
            fixed_bonus: None,
        })
    }

    pub fn fixed_bonus(mut self: Box<Self>, bonus: isize) -> Box<Self> {
        self.fixed_bonus = Some(bonus);
        self
    }
}

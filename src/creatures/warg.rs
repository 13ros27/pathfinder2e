use bevy::prelude::*;

use crate::{creatures::builder::prelude::*, weapons::prelude::*};

pub struct Warg;

impl SpawnCreature for Warg {
    fn spawn(self, world: &mut World) -> EntityWorldMut {
        BuildCreature::new(world)
            .name("Warg")
            .level(2)
            .perception(8)
            .stats(4, 3, 3, -1, 2, 2)
            .ac(17)
            .saves(11, 9, 6)
            .hp(36)
            .speed(40)
            .with_default::<Beast>()
            .add_action::<Step>(|a| a.cost(1))
            .add_action::<Stride>(|a| a.cost(1))
            .add_item::<Jaws>(|i| i.add_action::<Strike>(|a| a.cost(1).fixed_bonus(7)))
            .spawn()
    }
}

#[derive(Bundle, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Jaws {
    // TODO: Add GrabOnHit
    pub damage: BaseDamage,
    pub melee: Melee,
    pub innate: Innate,
}

impl Default for Jaws {
    fn default() -> Self {
        Self {
            damage: BaseDamage(Damage::new(D8, DamageType::Piercing)),
            melee: Melee,
            innate: Innate,
        }
    }
}

impl Named for Jaws {
    const NAME: &'static str = "Jaws";
}

mod builder;
mod goblin_commando;
mod goblin_warrior;
mod warg;

pub use goblin_commando::GoblinCommando;
pub use goblin_warrior::GoblinWarrior;
pub use warg::Warg;

use bevy::prelude::*;

use crate::Position;

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Creature;

pub trait SpawnCreature {
    fn spawn(self, commands: &mut World) -> EntityWorldMut;
}

pub trait SpawnCreatureExt {
    fn spawn_creature(
        &mut self,
        creature: impl SpawnCreature,
        position: Position,
    ) -> EntityWorldMut;
}

impl SpawnCreatureExt for World {
    fn spawn_creature(
        &mut self,
        creature: impl SpawnCreature,
        position: Position,
    ) -> EntityWorldMut {
        let creature = creature.spawn(self).insert(position).id();
        self.entity_mut(creature)
    }
}

use bevy::{
    ecs::component::{ComponentInfo, TableStorage},
    prelude::*,
};

use crate::{
    conditions::{ConditionCommandsExt, ValueCondition},
    creatures::Creature,
    relations::prelude::*,
    weapons::prelude::Named,
};

#[derive(Component, Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Dead;

#[derive(Component, Copy, Clone, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DeathThreshold(pub usize);

#[derive(Copy, Clone, Debug, Deref, DerefMut, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Dying(pub usize);

impl Named for Dying {
    const NAME: &'static str = "Dying";
}
impl ValueCondition for Dying {
    const ONE: Self = Dying(1);
}

impl Component for Dying {
    type Storage = TableStorage;
    fn init_component_info(info: &mut ComponentInfo) {
        // Increment Wounded by one when you lose Dying
        info.on_remove(|mut world, id, _| {
            let creature = *world.get::<Relation<Creature>>(id).unwrap();
            world
                .commands()
                .entity(*creature)
                .increment_condition::<Wounded>();
        });
    }
}

#[derive(Component, Copy, Clone, Debug, Deref, DerefMut, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Wounded(pub usize);

impl Named for Wounded {
    const NAME: &'static str = "Wounded";
}
impl ValueCondition for Wounded {
    const ONE: Self = Wounded(1);
}

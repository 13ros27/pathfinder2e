#![feature(
    exact_size_is_empty,
    iter_intersperse,
    int_roundings,
    let_chains,
    type_changing_struct_update
)]

mod actions;
mod cleanup;
mod conditions;
mod creature_info;
mod creatures;
mod damage;
mod dice;
mod dying;
mod flanking;
mod graphics;
mod held;
mod initiative;
mod movement;
mod names;
mod queue;
mod reactions;
mod relations;
mod simulation;
mod success;
mod weapons;

use bevy::{
    input::mouse::{MouseScrollUnit, MouseWheel},
    prelude::*,
    window::close_on_esc,
};

use creature_info::Position;
use creatures::*;
use damage::DealDamage;
use initiative::Initiative;
use simulation::{SimCleanup, SimTweak, SimulationSet};

use crate::{creatures::Warg, flanking::InReachOf, graphics::tiles::Tile};

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugins((
            actions::ActionPlugin,
            graphics::GraphicsPlugin,
            simulation::SimulationPlugin,
            weapons::WeaponTraitPlugin,
            movement::MovementPlugin,
            reactions::ReactionPlugin,
            cleanup::CleanupPlugin,
            queue::QueuePlugin,
        ))
        .insert_resource(Initiative::empty())
        .add_systems(Startup, setup)
        .add_systems(
            Update,
            (debug_input, move_camera, close_on_esc).before(SimulationSet),
        )
        .add_systems(SimTweak, DealDamage::apply)
        .add_systems(SimCleanup, InReachOf::recalculate)
        .run();
}

fn debug_input(
    world: &mut World,
    entities: &mut QueryState<
        Entity,
        (
            Without<Tile>,
            Without<Node>,
            Without<Camera>,
            Without<Window>,
        ),
    >,
) {
    if world
        .get_resource::<ButtonInput<KeyCode>>()
        .unwrap()
        .just_pressed(KeyCode::KeyP)
    {
        let entities: Vec<_> = entities.iter(world).collect();
        for entity in &entities {
            println!(
                "Entity {entity:?}: {:?}",
                world
                    .inspect_entity(*entity)
                    .iter()
                    .map(|i| i.name())
                    .collect::<Vec<_>>()
            );
        }
        println!("{} important entities", entities.len());
    }
}

fn move_camera(
    input: Res<ButtonInput<KeyCode>>,
    mut scroll: EventReader<MouseWheel>,
    time: Res<Time>,
    mut camera: Query<&mut Transform, With<Camera3d>>,
) {
    let mut camera = camera.single_mut();
    let forward = input.pressed(KeyCode::KeyW) as isize - input.pressed(KeyCode::KeyS) as isize;
    let right = input.pressed(KeyCode::KeyD) as isize - input.pressed(KeyCode::KeyA) as isize;

    let mut zoom = 0.0;
    for ev in scroll.read() {
        zoom += match ev.unit {
            MouseScrollUnit::Line => 20.0 * ev.y,
            MouseScrollUnit::Pixel => 2.0 * ev.y,
        };
    }

    let mut cam_forward = camera.forward();
    cam_forward.y = 0.0;
    let movement = cam_forward.normalize() * forward as f32
        + camera.right() * right as f32
        + camera.forward() * zoom;
    camera.translation += movement * 10.0 * time.delta_seconds();
}

fn setup(world: &mut World) {
    let gobbo1 = world.spawn_creature(GoblinCommando, Position(0, 0)).id();
    let gobbo2 = world.spawn_creature(GoblinWarrior, Position(3, 2)).id();
    let warg = world.spawn_creature(Warg, Position(-3, -3)).id();

    world
        .get_resource_mut::<Initiative>()
        .unwrap()
        .extend(vec![gobbo1, gobbo2, warg]);
}

#[derive(Component, Clone, Copy, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Level(isize);

#[derive(Component, Clone, Copy, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Perception(isize);

#[derive(Component, Clone, Copy, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Ac(usize);

#[derive(Component, Clone, Copy, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct MaxHP(usize);

#[derive(Component, Clone, Copy, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Hp(usize);

#[derive(Component, Clone, Copy, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Speed(usize);

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Action;

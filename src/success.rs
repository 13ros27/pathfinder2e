use bevy::prelude::*;
use std::cmp::{max, min};

use crate::dice::D20;

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CritSuccess;

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Success;

pub fn degrees_of_success(
    commands: &mut Commands,
    bonus: isize,
    target_dc: usize,
    mut crit_success: impl FnMut(&mut Commands),
    mut success: impl FnMut(&mut Commands),
    mut failure: impl FnMut(&mut Commands),
    mut crit_failure: impl FnMut(&mut Commands),
) {
    let dice_roll = D20.roll();
    let total_roll = dice_roll as isize + bonus;

    let mut degree = if total_roll >= target_dc as isize + 10 {
        3 // Critical Success
    } else if total_roll >= target_dc as isize {
        2 // Success
    } else if total_roll > target_dc as isize - 10 {
        1 // Failure
    } else {
        0 // Critical Failure
    };

    if dice_roll == 20 {
        degree = min(3, degree + 1);
    } else if dice_roll == 1 {
        degree = max(0, degree - 1);
    }

    match degree {
        3 => crit_success(commands),
        2 => success(commands),
        1 => failure(commands),
        0 => crit_failure(commands),
        _ => unreachable!(),
    }
}

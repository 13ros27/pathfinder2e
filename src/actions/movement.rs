use bevy::prelude::*;

use crate::{
    actions::ValidatedAction,
    creature_info::Position,
    creatures::Creature,
    movement::{MovementPlan, NoTrigger},
    names::Named,
    relations::prelude::*,
    simulation::{SimulationInput, TriggerAction},
    Action, Speed,
};

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Step;

impl Named for Step {
    const NAME: &'static str = "Step";
}

impl ValidatedAction for Step {
    type Input = (KindEntity<Action>, Position);
    type ExtraParams<'w, 's> = (
        Query<'w, 's, (Has<Step>, &'static Relation<Creature>), With<Action>>,
        Query<'w, 's, (&'static Position, &'static Speed), With<Creature>>,
    );

    fn validate(
        In((action, target_pos)): In<Self::Input>,
        (creature_rels, creature_info): Self::ExtraParams<'_, '_>,
    ) -> bool {
        let (has_step, &creature) = creature_rels.get(*action).unwrap();
        if has_step {
            let (self_pos, &speed) = creature_info.get(*creature).unwrap();
            if *speed < 10 {
                return false; // You cannot step if you have less than 10 feet of movement
            }
            self_pos.distance(&target_pos) == 5
        } else {
            false
        }
    }
}

impl Step {
    pub(crate) fn trigger(
        mut commands: Commands,
        trigger: Res<SimulationInput>,
        action_info: Query<(&Relation<Action>, &Position), With<TriggerAction>>,
        positions: RelationQuery<Creature, (Entity, &Position)>,
    ) {
        let (&action, &target_pos) = action_info.get(*trigger.unwrap()).unwrap();
        let (creature, &self_pos) = positions.get(*action).unwrap();

        // Spawn a NoTrigger movement plan consisting of our current position and our target
        commands
            .spawn((NoTrigger, MovementPlan(vec![self_pos, target_pos])))
            .insert_relation::<Creature>(creature);
    }
}

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Stride;

impl Named for Stride {
    const NAME: &'static str = "Stride";
}

impl ValidatedAction for Stride {
    type Input = (KindEntity<Action>, Position);
    type ExtraParams<'w, 's> = (
        Query<'w, 's, (Has<Stride>, &'static Relation<Creature>), With<Action>>,
        Query<'w, 's, (&'static Position, &'static Speed), With<Creature>>,
    );

    fn validate(
        In((action, target_pos)): In<Self::Input>,
        (creature_rels, creature_info): Self::ExtraParams<'_, '_>,
    ) -> bool {
        let (has_stride, &creature) = creature_rels.get(*action).unwrap();
        if has_stride {
            let (self_pos, &speed) = creature_info.get(*creature).unwrap();
            let distance = self_pos.distance(&target_pos);
            distance != 0 && distance <= *speed
        } else {
            false
        }
    }
}

impl Stride {
    pub(crate) fn trigger(
        mut commands: Commands,
        trigger: Res<SimulationInput>,
        action_info: Query<(&Relation<Action>, &Position), With<TriggerAction>>,
        positions: RelationQuery<Creature, (Entity, &Position)>,
    ) {
        let (&action, target_pos) = action_info.get(*trigger.unwrap()).unwrap();
        let (creature, self_pos) = positions.get(*action).unwrap();

        // Spawn a movement plan with our route between our current position and our target
        commands
            .spawn(MovementPlan(self_pos.route(target_pos)))
            .insert_relation::<Creature>(creature);
    }
}

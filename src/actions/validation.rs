use bevy::{
    ecs::{query::QueryData, system::SystemParam},
    prelude::*,
};

use crate::{
    creature_info::Position,
    creatures::Creature,
    relations::prelude::*,
    simulation::{SimulationInput, TriggerAction},
    Action,
};

pub trait ValidatedAction {
    type Input;
    type ExtraParams<'w, 's>: SystemParam;
    fn validate(input: In<Self::Input>, query: Self::ExtraParams<'_, '_>) -> bool;
}

pub trait SimulationValidation {
    type InputParams<'w, 's>: QueryData;
    type ExtraParams<'w, 's>: SystemParam;
    fn validate_sim(
        trigger: Res<SimulationInput>,
        input: Query<Self::InputParams<'_, '_>, With<TriggerAction>>,
        query: Self::ExtraParams<'_, '_>,
    ) -> bool;
}

/// Default implementation for an action targeting a position
impl<A: ValidatedAction<Input = (KindEntity<Action>, Position)>> SimulationValidation
    for (A, (KindEntity<Action>, Position))
{
    type InputParams<'w, 's> = (&'static Relation<Action>, &'static Position);
    type ExtraParams<'w, 's> = A::ExtraParams<'w, 's>;
    fn validate_sim(
        trigger: Res<SimulationInput>,
        input: Query<(&'static Relation<Action>, &'static Position), With<TriggerAction>>,
        query: A::ExtraParams<'_, '_>,
    ) -> bool {
        if let Some(input_trigger) = **trigger
            && let Ok((&action, &target_pos)) = input.get(*input_trigger)
        {
            A::validate(In((action.as_weak(), target_pos)), query)
        } else {
            false
        }
    }
}

/// Default implementation for action targeting a creature
impl<A: ValidatedAction<Input = (KindEntity<Action>, KindEntity<Creature>)>> SimulationValidation
    for (A, (KindEntity<Action>, KindEntity<Creature>))
{
    type InputParams<'w, 's> = (&'static Relation<Action>, &'static Relation<Creature>);
    type ExtraParams<'w, 's> = A::ExtraParams<'w, 's>;
    fn validate_sim(
        trigger: Res<SimulationInput>,
        input: Query<(&'static Relation<Action>, &'static Relation<Creature>), With<TriggerAction>>,
        query: A::ExtraParams<'_, '_>,
    ) -> bool {
        if let Some(input_trigger) = **trigger
            && let Ok((&action, &target)) = input.get(*input_trigger)
        {
            A::validate(In((action.as_weak(), target.as_weak())), query)
        } else {
            false
        }
    }
}

/// Workaround for no monomorphization over associated types
impl<I, A> SimulationValidation for A
where
    A: ValidatedAction<Input = I>,
    (A, I): SimulationValidation,
{
    type InputParams<'w, 's> = <(A, I) as SimulationValidation>::InputParams<'w, 's>;
    type ExtraParams<'w, 's> = <(A, I) as SimulationValidation>::ExtraParams<'w, 's>;
    fn validate_sim(
        trigger: Res<SimulationInput>,
        input: Query<Self::InputParams<'_, '_>, With<TriggerAction>>,
        query: Self::ExtraParams<'_, '_>,
    ) -> bool {
        <(A, I)>::validate_sim(trigger, input, query)
    }
}

use bevy::{
    ecs::component::{ComponentInfo, TableStorage},
    prelude::*,
};

use super::{AttackDamage, Constant, StatValue};
use crate::{
    creature_info::Stats,
    creatures::Creature,
    damage::DealDamage,
    dice::Dice,
    queue::{Queued, ShortLived},
    relations::prelude::*,
    Action,
};

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ActionResult;

impl Component for ActionResult {
    type Storage = TableStorage;
    fn init_component_info(info: &mut ComponentInfo) {
        info.on_add(|mut w, e, _| {
            w.commands().entity(e).insert(ShortLived);
        });
    }
}

impl ActionResult {
    pub fn apply(
        mut commands: Commands,
        query: Query<
            (
                &Relation<Creature>,
                &Relation<AttackDamage>,
                &Relation<Action>,
            ),
            (With<ActionResult>, Without<Queued>),
        >,
        mut damages: Query<(Entity, &mut Constant, Option<&StatValue>, &Dice), With<AttackDamage>>,
        creature_stats: RelationQuery<Creature, Stats>,
    ) {
        for (&target_creature, &attack_damage, &action) in &query {
            let (damage_ent, mut damage, stat_damage, damage_dice) =
                damages.get_mut(*attack_damage).unwrap();

            let damage_roll = damage_dice.roll();
            commands.entity(damage_ent).remove::<Dice>();
            **damage += damage_roll as isize;

            if let Some(stat_damage) = stat_damage {
                **damage += stat_damage.get_bonus(creature_stats.get(*action).unwrap());
            }

            info!("Dealing {} damage", **damage);

            commands
                .spawn((Queued, target_creature))
                .own_relation::<DealDamage, _>(attack_damage);
        }
    }
}

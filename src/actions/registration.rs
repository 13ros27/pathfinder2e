use bevy::{
    ecs::{component::ComponentId, system::SystemId},
    prelude::*,
    utils::HashMap,
};
use std::mem;

use crate::actions::ValidatedAction;

#[derive(Resource, Clone, Debug, Default, PartialEq, Eq)]
pub struct ValidActions(HashMap<ComponentId, Entity>);

impl ValidActions {
    fn insert<A>(&mut self, world: &mut World, system: SystemId<A::Input, bool>)
    where
        A: Component + ValidatedAction,
    {
        world.init_component::<A>();

        // SAFETY: SystemId is just an Entity and PhantomData so this should work but it probably isn't technically sound.
        // This is so extremely cursed (maybe PR some unsafe methods on SystemId for this?)
        let system_entity: Entity = unsafe { mem::transmute(system) };
        self.0
            .insert(world.component_id::<A>().unwrap(), system_entity);
    }

    pub fn get<A>(&self, world: &World) -> SystemId<A::Input, bool>
    where
        A: Component + ValidatedAction,
    {
        let system_entity: Entity = *self.0.get(&world.component_id::<A>().unwrap()).unwrap();
        // SAFETY: As this acts on private data, we know this must align with the input from above (again, very cursed)
        unsafe { mem::transmute(system_entity) }
    }
}

pub(crate) trait RegisterActionExt {
    fn register_action<A>(
        &mut self,
        validation: impl System<In = A::Input, Out = bool>,
    ) -> &mut Self
    where
        A: Component + ValidatedAction;
}

impl RegisterActionExt for App {
    fn register_action<A>(
        &mut self,
        validation: impl System<In = A::Input, Out = bool>,
    ) -> &mut Self
    where
        A: Component + ValidatedAction,
    {
        self.world
            .resource_scope(|world, mut actions: Mut<ValidActions>| {
                let id = world.register_system(validation);
                actions.insert::<A>(world, id);
            });
        self
    }
}

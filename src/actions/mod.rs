mod action_result;
mod movement;
mod registration;
mod strike;
mod validation;

use bevy::{
    ecs::component::{ComponentInfo, TableStorage},
    prelude::*,
};
use std::{fmt::Debug, ops::MulAssign};

use crate::{
    creature_info::{stats::StatsItem, StatsEnum},
    queue::{Queued, ShortLived},
    relations::prelude::*,
    simulation::{SimFinish, SimStart, SimTweak},
    Action,
};
use registration::RegisterActionExt;

pub mod all {
    pub use super::movement::{Step, Stride};
    pub use super::strike::Strike;
}

pub use action_result::ActionResult;
pub use movement::{Step, Stride};
pub use registration::ValidActions;
pub use strike::Strike;
pub use validation::{SimulationValidation, ValidatedAction};

pub struct ActionPlugin;

impl Plugin for ActionPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<ValidActions>()
            .register_action::<Step>(IntoSystem::into_system(Step::validate))
            .register_action::<Stride>(IntoSystem::into_system(Stride::validate))
            .register_action::<Strike>(IntoSystem::into_system(Strike::validate))
            .add_systems(
                SimStart,
                (
                    Step::trigger.run_if(Step::validate_sim),
                    Stride::trigger.run_if(Stride::validate_sim),
                    Strike::trigger.run_if(Strike::validate_sim),
                ),
            )
            .add_systems(SimTweak, (ActionResult::apply, apply_fixed_attack_bonus))
            .add_systems(SimFinish, Strike::apply);
    }
}

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ActionCost(pub usize);

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DoingAction;

impl Component for DoingAction {
    type Storage = TableStorage;
    fn init_component_info(info: &mut ComponentInfo) {
        info.on_add(|mut w, e, _| {
            w.commands().entity(e).insert(ShortLived);
        });
    }
}

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct AttackBonus;

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct AttackDamage;

#[derive(Component, Clone, Copy, Debug, Deref, DerefMut, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Constant(pub isize);

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct StatValue {
    pub stat: StatsEnum,
    multiplier: i8,
}

impl StatValue {
    pub fn new(stat: StatsEnum) -> StatValue {
        StatValue {
            stat,
            multiplier: 1,
        }
    }

    pub fn set_stat(&mut self, stat: StatsEnum) {
        self.stat = stat;
    }

    fn get_bonus(&self, stats: StatsItem) -> isize {
        self.multiplier as isize
            * match self.stat {
                StatsEnum::Str => **stats.str,
                StatsEnum::Dex => **stats.dex,
                StatsEnum::Con => **stats.con,
                StatsEnum::Int => **stats.int,
                StatsEnum::Wis => **stats.wis,
                StatsEnum::Cha => **stats.cha,
            }
    }
}

impl MulAssign<i8> for StatValue {
    fn mul_assign(&mut self, rhs: i8) {
        self.multiplier *= rhs;
    }
}

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct FixedAttackBonus(pub isize);

fn apply_fixed_attack_bonus(
    current_actions: Query<
        (&Relation<Action>, &Relation<AttackBonus>),
        (With<DoingAction>, Without<Queued>),
    >,
    bonuses: Query<&FixedAttackBonus, With<Action>>,
    mut const_bonuses: Query<&mut Constant, With<AttackBonus>>,
) {
    for (&action, attack_bonus) in &current_actions {
        if let Ok(FixedAttackBonus(fixed_bonus)) = bonuses.get(*action) {
            let mut const_bonus = const_bonuses.get_mut(**attack_bonus).unwrap();
            const_bonus.0 += fixed_bonus;
        }
    }
}

#[derive(Component, Clone, Copy, Debug, Default, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct MultiAttackPenalty(usize); // 0, 1 or 2

impl MultiAttackPenalty {
    /// Get the multi-attack penalty, depending on if the attack is agile.
    pub fn penalty(&self, agile: bool) -> isize {
        if agile {
            -(self.0 as isize * 4)
        } else {
            -(self.0 as isize * 5)
        }
    }

    /// Increment the multi-attack penalty.
    pub fn increment(&mut self) {
        if self.0 < 2 {
            self.0 += 1;
        }
    }
}

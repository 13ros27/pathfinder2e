use bevy::prelude::*;

use super::{AttackBonus, AttackDamage, Constant, DoingAction, MultiAttackPenalty, StatValue};
use crate::actions::{ActionResult, ValidatedAction};
use crate::creature_info::{Position, Stats, StatsEnum};
use crate::creatures::Creature;
use crate::damage::DamageType;
use crate::dice::Dice;
use crate::dying::Dead;
use crate::names::Named;
use crate::queue::Queued;
use crate::relations::prelude::*;
use crate::simulation::{SimulationInput, TriggerAction};
use crate::success::{degrees_of_success, CritSuccess, Success};
use crate::weapons::prelude::*;
use crate::{Ac, Action};

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Strike;

impl Named for Strike {
    const NAME: &'static str = "Strike";
}

impl ValidatedAction for Strike {
    type Input = (KindEntity<Action>, KindEntity<Creature>);
    type ExtraParams<'w, 's> = (
        Query<'w, 's, (Has<Strike>, &'static Relation<Creature>), With<Action>>,
        Query<'w, 's, &'static Position, With<Creature>>,
        Query<'w, 's, (), (With<Creature>, With<Dead>)>,
        RelationQuery<'w, 's, Item, (Option<&'static Ranged>, Has<Reach>)>,
    );

    fn validate(
        In((action, target)): In<Self::Input>,
        (creature_rels, positions, dead_creatures, weapon_info): Self::ExtraParams<'_, '_>,
    ) -> bool {
        let (has_strike, &creature) = creature_rels.get(*action).unwrap();
        if has_strike {
            if *creature == *target || dead_creatures.contains(*target) {
                return false; // Can't target yourself or a dead creature
            }

            let self_pos = positions.get(*creature).unwrap();
            let target_pos = positions.get(*target).unwrap();

            let (ranged, has_reach) = weapon_info.get(*action).unwrap();
            if let Some(ranged) = ranged {
                // Can target with ranged if the target is within 6 * range increment (inclusive)
                self_pos.distance(target_pos) <= 6 * ranged.range_increment
            } else {
                // TODO: Add support for creature reach
                let reach_distance = if has_reach { 10 } else { 5 };
                self_pos.distance_reach(target_pos) <= reach_distance
            }
        } else {
            false
        }
    }
}

impl Strike {
    pub fn trigger(
        mut commands: Commands,
        trigger: Res<SimulationInput>,
        action_info: Query<(&Relation<Action>, &Relation<Creature>), With<TriggerAction>>,
        mut creatures: RelationQuery<Creature, (&mut MultiAttackPenalty, &Position)>,
        weapon_info: RelationQuery<Item, (&BaseDamage, Option<&Ranged>, Has<Agile>)>,
        positions: Query<&Position, With<Creature>>,
    ) {
        let (&action, &target) = action_info.get(*trigger.unwrap()).unwrap();
        let (base_damage, ranged, is_agile) = weapon_info.get(*action).unwrap();

        let (mut map, self_pos) = creatures.get_mut(*action).unwrap();
        let target_pos = positions.get(*target).unwrap();

        let attack_bonus = commands
            .spawn(if let Some(ranged) = ranged {
                let increment_penalty = ranged.increment_penalty(self_pos.distance(target_pos));

                // Spawn a dexterity attack roll with map and ranged increment penalties
                (
                    Constant(increment_penalty + map.penalty(is_agile)),
                    StatValue::new(StatsEnum::Dex),
                )
            } else {
                // Spawn a strength attack roll with multi-attack penalty
                (
                    Constant(map.penalty(is_agile)),
                    StatValue::new(StatsEnum::Str),
                )
            })
            .id();

        // Spawn the attack damage instance with base damage dice and type
        let mut attack_damage = commands.spawn((
            AttackDamage,
            Constant(0),
            base_damage.dice,
            base_damage.damage_type,
        ));
        if ranged.is_none() {
            // Melee attacks also deal strength damage
            attack_damage.insert(StatValue::new(StatsEnum::Str));
        }
        let attack_damage = attack_damage.id();

        map.increment(); // Increment the multi-attack penalty

        // Spawn a DoingAction to trigger Strike::apply
        commands
            .spawn((DoingAction, action, target))
            .insert_relation::<AttackDamage>(attack_damage)
            .insert_owned_bidir_relation::<Strike, AttackBonus>(attack_bonus);
    }

    pub fn apply(
        mut commands: Commands,
        query: Query<
            (
                &Relation<Action>,
                &Relation<Creature>,
                &Relation<AttackDamage>,
                &Relation<AttackBonus>,
            ),
            (With<DoingAction>, With<Strike>),
        >,
        creature_stats: RelationQuery<Creature, Stats>,
        mut damages: Query<
            (
                &mut Constant,
                Option<&mut StatValue>,
                &mut Dice,
                &DamageType,
            ),
            (With<AttackDamage>, Without<AttackBonus>),
        >,
        bonuses: Query<(&Constant, &StatValue), (With<AttackBonus>, Without<AttackDamage>)>,
        acs: Query<&Ac, With<Creature>>,
    ) {
        for (&action, &target, &damage, &bonus_rel) in query.iter() {
            let (mut const_damage, mut stat_damage, mut dice_damage, damage_type) =
                damages.get_mut(*damage).unwrap();
            let (const_bonus, stat_bonus) = bonuses.get(*bonus_rel).unwrap();
            let stats = creature_stats.get(*action).unwrap();
            let bonus = **const_bonus + stat_bonus.get_bonus(stats);

            if let Some(ref stat_damage) = stat_damage {
                bevy::utils::tracing::info!(
                    "Rolling with a +{bonus}, dealing {} + {} + {} {damage_type} damage",
                    *dice_damage,
                    stat_damage.stat,
                    **const_damage
                );
            } else {
                bevy::utils::tracing::info!(
                    "Rolling with a +{bonus}, dealing {} + {} {damage_type} damage",
                    *dice_damage,
                    **const_damage
                );
            }

            // Roll 1d20 + bonus and find the degree of success against the targets AC
            degrees_of_success(
                &mut commands,
                bonus,
                **acs.get(*target).unwrap(),
                |c| {
                    // Deal double damage on a critical success
                    **const_damage *= 2;
                    *dice_damage *= 2;
                    if let Some(ref mut stat_damage) = stat_damage {
                        **stat_damage *= 2;
                    }
                    c.spawn((Queued, CritSuccess, ActionResult, damage, target, action));
                },
                |c| {
                    c.spawn((Queued, Success, ActionResult, damage, target, action));
                },
                |c| c.entity(*damage).despawn(), // Do nothing on a failure or critical failure
                |c| c.entity(*damage).despawn(),
            );
        }
    }
}

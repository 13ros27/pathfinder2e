use bevy::{ecs::system::SystemId, prelude::*};

use crate::{
    creature_info::Position,
    creatures::Creature,
    graphics::{hovered::Hovered, tiles::Tile},
    relations::prelude::*,
    simulation::{SimulationEvent, TriggerAction},
    Action,
};

#[derive(Component)]
pub struct AwaitingInput;

#[derive(Component)]
pub struct PositionInput;

#[derive(Component, Copy, Clone)]
pub struct InputValidation<I> {
    pub system: SystemId<I, bool>,
}

impl<I: 'static> InputValidation<I> {
    pub fn validate(&self, world: &mut World, input: I) -> bool {
        world.run_system_with_input(self.system, input).unwrap()
    }
}

pub fn position_validation(
    world: &mut World,
    hovered_tile: &mut QueryState<(Entity, &Position), (With<Tile>, With<Hovered>)>,
    triggered: &mut QueryState<(Entity, &Relation<Action>), With<TriggerAction>>,
    actions: &mut QueryState<&InputValidation<(KindEntity<Action>, Position)>, With<Action>>,
    tile_material: &mut QueryState<&mut Handle<StandardMaterial>, With<Tile>>,
) {
    if let Some((tile, &target_pos)) = hovered_tile.iter(world).next() {
        let triggered: Vec<_> = triggered.iter(world).map(|(e, &a)| (e, a)).collect(); // Work around for mutable exclusivity
        for (trigger_ent, action) in triggered {
            // Checking this is a position action
            if let Ok(&validation) = actions.get(world, *action) {
                // Cancel an action with a right click
                if world
                    .get_resource::<ButtonInput<MouseButton>>()
                    .unwrap()
                    .just_pressed(MouseButton::Right)
                {
                    world.entity_mut(trigger_ent).despawn();
                }

                let action = action.as_weak();
                let valid = validation.validate(world, (action, target_pos));

                let mut materials = world
                    .get_resource_mut::<Assets<StandardMaterial>>()
                    .unwrap();
                let color = if valid {
                    let material = materials.add(Color::GREEN);

                    if world
                        .get_resource::<ButtonInput<MouseButton>>()
                        .unwrap()
                        .just_pressed(MouseButton::Left)
                    {
                        let event = world.entity_mut(trigger_ent).insert(target_pos).id();
                        world.send_event(SimulationEvent::new(event));
                    }

                    material
                } else {
                    materials.add(Color::RED)
                };

                let mut material = tile_material.get_mut(world, tile).unwrap();
                *material = color;
            }
        }
    }
}

pub fn creature_validation(
    world: &mut World,
    hovered_creature: &mut QueryState<Entity, (With<Creature>, With<Hovered>)>,
    triggered: &mut QueryState<(Entity, &Relation<Action>), With<TriggerAction>>,
    actions: &mut QueryState<
        &InputValidation<(KindEntity<Action>, KindEntity<Creature>)>,
        With<Action>,
    >,
    creature_material: &mut QueryState<&mut Handle<StandardMaterial>, With<Creature>>,
) {
    if let Some(creature) = hovered_creature.iter(world).next() {
        let triggered: Vec<_> = triggered.iter(world).map(|(e, &a)| (e, a)).collect(); // Work around for mutable exclusivity
        for (trigger_ent, action) in triggered {
            // Checking this is a position action
            if let Ok(&validation) = actions.get(world, *action) {
                // Cancel an action with a right click
                if world
                    .get_resource::<ButtonInput<MouseButton>>()
                    .unwrap()
                    .just_pressed(MouseButton::Right)
                {
                    world.entity_mut(trigger_ent).despawn();
                }

                let action = action.as_weak();
                let valid = validation.validate(world, (action, KindEntity::new(creature)));

                let mut materials = world
                    .get_resource_mut::<Assets<StandardMaterial>>()
                    .unwrap();
                let color = if valid {
                    let material = materials.add(Color::GREEN);

                    if world
                        .get_resource::<ButtonInput<MouseButton>>()
                        .unwrap()
                        .just_pressed(MouseButton::Left)
                    {
                        let event = world
                            .entity_mut(trigger_ent)
                            .insert(Relation::<Creature>::new_unchecked(creature))
                            .id();
                        world.send_event(SimulationEvent::new(event));
                    }

                    material
                } else {
                    materials.add(Color::RED)
                };

                let mut material = creature_material.get_mut(world, creature).unwrap();
                *material = color;
            }
        }
    }
}

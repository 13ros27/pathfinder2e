use bevy::prelude::*;

use crate::{
    graphics::reactions::{ReactionPopup, ReactionResponse},
    initiative::Initiative,
    relations::prelude::*,
    Action,
};

#[derive(Component, Clone, Copy, Debug, Deref, DerefMut, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ActionButtonId(usize);

#[derive(Component, Clone, Copy, Debug, Deref, DerefMut, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ActionButton(pub KindEntity<Action>);

pub fn ui_setup(mut commands: Commands) {
    // Action Buttons
    commands
        .spawn(NodeBundle {
            style: Style {
                width: Val::Percent(100.0),
                height: Val::Percent(10.0),
                justify_content: JustifyContent::SpaceEvenly,
                top: Val::Percent(90.0),
                ..default()
            },
            ..default()
        })
        .with_children(|parent| {
            for i in 0..5 {
                parent
                    .spawn(ButtonBundle {
                        style: Style {
                            width: Val::Px(250.0),
                            height: Val::Px(45.0),
                            border: UiRect::all(Val::Px(5.0)),
                            justify_content: JustifyContent::Center,
                            ..default()
                        },
                        border_color: BorderColor(Color::BLACK),
                        background_color: Color::rgb(0.15, 0.15, 0.15).into(),
                        ..default()
                    })
                    .with_children(|parent| {
                        parent.spawn(TextBundle::from_section(
                            "",
                            TextStyle {
                                font_size: 20.0,
                                color: Color::rgb(0.9, 0.9, 0.9),
                                ..default()
                            },
                        ));
                    })
                    .insert(ActionButtonId(i));
            }
        });

    // Reaction popup
    commands
        .spawn(NodeBundle {
            style: Style {
                width: Val::Percent(30.0),
                height: Val::Percent(60.0),
                flex_direction: FlexDirection::Column,
                align_items: AlignItems::Center,
                justify_content: JustifyContent::SpaceEvenly,
                top: Val::Percent(15.0),
                left: Val::Percent(35.0),
                border: UiRect::all(Val::Px(5.0)),
                display: Display::None, // Start hidden
                ..default()
            },
            background_color: BackgroundColor(Color::rgba(0.0, 0.0, 0.0, 0.75)),
            border_color: BorderColor(Color::WHITE),
            ..default()
        })
        .with_children(|parent| {
            parent.spawn(TextBundle::from_section(
                "Reaction",
                TextStyle {
                    font_size: 40.0,
                    ..default()
                },
            ));
            parent.spawn(TextBundle::from_section(
                "Do you want to use the '' reaction?",
                TextStyle {
                    font_size: 25.0,
                    ..default()
                },
            ));
            parent
                .spawn(ButtonBundle {
                    style: Style {
                        width: Val::Px(250.0),
                        height: Val::Px(45.0),
                        border: UiRect::all(Val::Px(5.0)),
                        justify_content: JustifyContent::Center,
                        ..default()
                    },
                    border_color: BorderColor(Color::BLACK),
                    background_color: Color::rgb(0.15, 0.15, 0.15).into(),
                    ..default()
                })
                .with_children(|parent| {
                    parent.spawn(TextBundle::from_section(
                        "Yes",
                        TextStyle {
                            font_size: 30.0,
                            ..default()
                        },
                    ));
                })
                .insert(ReactionResponse(true));
            parent
                .spawn(ButtonBundle {
                    style: Style {
                        width: Val::Px(250.0),
                        height: Val::Px(45.0),
                        border: UiRect::all(Val::Px(5.0)),
                        justify_content: JustifyContent::Center,
                        ..default()
                    },
                    border_color: BorderColor(Color::BLACK),
                    background_color: Color::rgb(0.15, 0.15, 0.15).into(),
                    ..default()
                })
                .with_children(|parent| {
                    parent.spawn(TextBundle::from_section(
                        "No",
                        TextStyle {
                            font_size: 30.0,
                            ..default()
                        },
                    ));
                })
                .insert(ReactionResponse(false));
        })
        .insert(ReactionPopup);

    // End Turn
    commands
        .spawn(ButtonBundle {
            style: Style {
                width: Val::Px(150.0),
                height: Val::Px(45.0),
                border: UiRect::all(Val::Px(5.0)),
                top: Val::Percent(2.5),
                left: Val::Percent(85.0),
                ..default()
            },
            border_color: BorderColor(Color::BLACK),
            background_color: Color::rgb(0.15, 0.15, 0.15).into(),
            ..default()
        })
        .with_children(|parent| {
            parent.spawn(TextBundle::from_section(
                "End Turn",
                TextStyle {
                    font_size: 32.0,
                    color: Color::rgb(0.9, 0.9, 0.9),
                    ..default()
                },
            ));
        })
        .insert(EndTurnButton);
}

pub fn button_highlighting(
    mut interaction_query: Query<
        (&Interaction, &mut BackgroundColor, &mut BorderColor),
        (Changed<Interaction>, With<Button>),
    >,
) {
    for (interaction, mut color, mut border_color) in &mut interaction_query {
        match *interaction {
            Interaction::Pressed => {
                *color = Color::rgb(0.35, 0.75, 0.35).into();
                border_color.0 = Color::RED;
            }
            Interaction::Hovered => {
                *color = Color::rgb(0.25, 0.25, 0.25).into();
                border_color.0 = Color::WHITE;
            }
            Interaction::None => {
                *color = Color::rgb(0.15, 0.15, 0.15).into();
                border_color.0 = Color::BLACK;
            }
        }
    }
}

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct EndTurnButton;

pub fn end_turn(
    interaction_query: Query<&Interaction, (Changed<Interaction>, With<EndTurnButton>)>,
    mut initiative: ResMut<Initiative>,
) {
    for interaction in &interaction_query {
        if interaction == &Interaction::Pressed {
            initiative.progress();
            println!("End turn");
        }
    }
}

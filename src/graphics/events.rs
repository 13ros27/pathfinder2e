use bevy::prelude::*;
use std::{collections::VecDeque, time::Duration};

use crate::{
    creatures::Creature, movement::MovementPlan, reactions::ApplyReaction, relations::prelude::*,
};

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GraphicsEventInfo;

#[derive(Resource, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GraphicsEvents(VecDeque<KindEntity<GraphicsEventInfo>>);

impl GraphicsEvents {
    pub fn send_movement(
        &mut self,
        commands: &mut Commands,
        creature: Relation<Creature>,
        movement: MovementPlan,
    ) {
        let graphics_info = commands
            .spawn((GraphicsEventInfo, GraphicsMovement(movement), creature))
            .id();
        self.0.push_back(KindEntity::new(graphics_info));
    }

    pub fn send_reaction(&mut self, commands: &mut Commands, reaction: KindEntity<ApplyReaction>) {
        commands.entity(*reaction).insert(GraphicsEventInfo);
        self.0.push_back(KindEntity::new(*reaction));
    }

    pub fn send_damage(
        &mut self,
        commands: &mut Commands,
        damage: usize,
        creature: Relation<Creature>,
    ) {
        let graphics_info = commands
            .spawn((GraphicsEventInfo, GraphicsDamage(damage), creature))
            .id();
        self.0.push_back(KindEntity::new(graphics_info));
    }

    pub fn current_event(&self) -> Option<&KindEntity<GraphicsEventInfo>> {
        self.0.front()
    }

    pub fn progress(&mut self, commands: &mut Commands) {
        if let Some(ent) = self.0.pop_front() {
            commands.entity(*ent).despawn();
        }
    }

    pub fn progress_leak(&mut self, commands: &mut Commands) {
        if let Some(ent) = self.0.pop_front() {
            commands.entity(*ent).remove::<GraphicsEventInfo>();
        }
    }
}

#[derive(Component, Clone, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GraphicsMovement(MovementPlan);

impl GraphicsMovement {
    pub fn apply(
        mut commands: Commands,
        mut events: ResMut<GraphicsEvents>,
        time: Res<Time>,
        mut progress: Local<Option<(usize, Duration)>>,
        query: Query<(&Relation<Creature>, &GraphicsMovement), With<GraphicsEventInfo>>,
        mut creatures: Query<&mut Transform, With<Creature>>,
    ) {
        if let Some(&event) = events.current_event()
            && let Ok((&creature, plan)) = query.get(*event)
        {
            let mut transform = creatures.get_mut(*creature).unwrap();

            if let Some((step, last_step_time)) = *progress {
                if last_step_time + Duration::from_millis(250) < time.elapsed() {
                    *transform = plan[step + 1].as_transform();

                    if step < plan.len() - 2 {
                        *progress = Some((step + 1, time.elapsed()));
                    } else {
                        *progress = None;
                        events.progress(&mut commands);
                    }
                }
            } else {
                *transform = plan[1].as_transform();
                if plan.len() != 2 {
                    *progress = Some((1, time.elapsed()));
                } else {
                    events.progress(&mut commands);
                }
            }
        }
    }
}

#[derive(Component, Clone, Copy, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GraphicsDamage(usize);

impl GraphicsDamage {
    pub fn apply(
        mut commands: Commands,
        mut events: ResMut<GraphicsEvents>,
        time: Res<Time>,
        mut progress: Local<Option<(Duration, KindEntity<Style>, KindEntity<Creature>)>>,
        query: Query<(&Relation<Creature>, &GraphicsDamage), With<GraphicsEventInfo>>,
        creature_transforms: Query<&Transform, With<Creature>>,
        camera: Query<(&Camera, &GlobalTransform)>,
        mut styles: Query<&mut Style, With<Text>>,
    ) {
        let (target, text_entity, damage) =
            if let Some((start_time, text_entity, target)) = *progress {
                if start_time + Duration::from_secs(1) < time.elapsed() {
                    commands.entity(*text_entity).despawn();
                    *progress = None;
                    return;
                } else {
                    (target, Some(text_entity), None)
                }
            } else if let Some(&event) = events.current_event()
                && let Ok((&target, &damage)) = query.get(*event)
            {
                events.progress(&mut commands);
                (target.as_weak(), None, Some(damage))
            } else {
                return;
            };

        let world_pos = creature_transforms.get(*target).unwrap().translation;
        let (camera, cam_transform) = camera.single();
        if let Some(coords) = camera.world_to_viewport(cam_transform, world_pos) {
            if progress.is_none() {
                let text_ent = commands
                    .spawn(TextBundle {
                        style: Style {
                            width: Val::Px(100.0),
                            height: Val::Px(50.0),
                            align_items: AlignItems::Center,
                            left: Val::Px(coords.x - 10.0),
                            top: Val::Px(coords.y - 35.0),
                            ..default()
                        },
                        text: Text::from_section(
                            format!("{}", *damage.unwrap()),
                            TextStyle {
                                font_size: 30.0,
                                color: Color::RED,
                                ..default()
                            },
                        ),
                        ..default()
                    })
                    .id();
                *progress = Some((time.elapsed(), KindEntity::<Style>::new(text_ent), target))
            } else {
                let mut style = styles.get_mut(*text_entity.unwrap()).unwrap();
                style.left = Val::Px(coords.x - 10.0);
                style.top = Val::Px(coords.y - 35.0);
            }
        }
    }
}

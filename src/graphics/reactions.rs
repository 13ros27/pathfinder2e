use bevy::prelude::*;

use crate::{
    graphics::events::{GraphicsEventInfo, GraphicsEvents},
    names::FixedName,
    reactions::{ApplyReaction, Reaction},
    relations::prelude::*,
    simulation::{SimulationEvent, TriggerAction},
};

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ReactionPopup;

#[derive(Component, Clone, Copy, Debug, Deref, DerefMut, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ReactionResponse(pub bool);

pub fn show_reaction_popup(
    events: Res<GraphicsEvents>,
    reactions: Query<&Relation<Reaction>, (With<ApplyReaction>, With<GraphicsEventInfo>)>,
    reaction_names: Query<&FixedName, With<Reaction>>,
    mut reaction_popup: Query<(&Children, &mut Style), With<ReactionPopup>>,
    mut text_query: Query<&mut Text>,
) {
    if let Some(&event) = events.current_event()
        && let Ok(&reaction) = reactions.get(*event)
    {
        let name = reaction_names.get(*reaction).unwrap();
        let (popup_children, mut style) = reaction_popup.single_mut();
        style.display = Display::Flex;
        let mut popup_text = text_query.get_mut(popup_children[1]).unwrap();
        popup_text.sections[0].value = format!("Do you want to use the '{}' reaction?", **name);
    }
}

pub fn reaction_response(
    mut commands: Commands,
    interaction_query: Query<
        (&Interaction, &ReactionResponse),
        (Changed<Interaction>, With<Button>),
    >,
    mut sim_event: EventWriter<SimulationEvent>,
    reaction: Query<Entity, With<ApplyReaction>>,
    mut graphics_events: ResMut<GraphicsEvents>,
    mut reaction_popup: Query<&mut Style, With<ReactionPopup>>,
) {
    if !reaction.is_empty() {
        for (interaction, &response) in &interaction_query {
            if interaction == &Interaction::Pressed {
                let mut style = reaction_popup.single_mut();
                style.display = Display::None;
                graphics_events.progress_leak(&mut commands);

                if *response {
                    println!("Accepted Reaction");
                    let event = commands
                        .spawn(TriggerAction)
                        .insert_relation::<ApplyReaction>(reaction.single())
                        .id();
                    sim_event.send(SimulationEvent::new(event));
                } else {
                    println!("Rejected Reaction");
                    commands.entity(reaction.single()).despawn();
                }
            }
        }
    }
}

use bevy::prelude::*;

use crate::{
    creatures::Creature,
    graphics::ui::{ActionButton, ActionButtonId},
    initiative::Initiative,
    names::FixedName,
    relations::prelude::*,
    simulation::TriggerAction,
    Action,
};

// Change action buttons from None to Some, could instead be an observer?
pub fn update_action_buttons(
    mut commands: Commands,
    initiative: Res<Initiative>,
    actions: Query<&Relations<Action>, With<Creature>>,
    mut action_buttons: Query<(Entity, &ActionButtonId, Option<&mut ActionButton>), With<Button>>,
) {
    let mut actions: Vec<_> = actions.get(*initiative.current()).unwrap().iter().collect();
    actions.sort();
    for (entity, &id, button) in &mut action_buttons {
        if let Some(&action) = actions.get(*id) {
            if let Some(mut button) = button {
                *button = ActionButton(action.as_weak());
            } else {
                commands
                    .entity(entity)
                    .insert(ActionButton(action.as_weak()));
            }
        } else if button.is_some() {
            commands.entity(entity).remove::<ActionButton>();
        }
    }
}

pub fn set_action_names(
    action_buttons: Query<(&ActionButton, &Children), With<Button>>,
    mut text_query: Query<&mut Text>,
    names: Query<&FixedName, With<Action>>,
) {
    for (&action, child) in &action_buttons {
        let mut text = text_query.get_mut(child[0]).unwrap();
        text.sections[0].value = names.get(**action).unwrap().to_string();
    }
}

pub fn trigger_actions(
    mut commands: Commands,
    interaction_query: Query<(&Interaction, &ActionButton), (Changed<Interaction>, With<Button>)>,
) {
    for (interaction, &action) in &interaction_query {
        if interaction == &Interaction::Pressed {
            println!("Trigger Action {:?}", *action);
            commands.spawn((TriggerAction, Relation::<Action>::new_unchecked(**action)));
        }
    }
}

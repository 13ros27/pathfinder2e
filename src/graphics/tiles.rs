use bevy::{
    prelude::*,
    render::{
        mesh::Indices, render_asset::RenderAssetPersistencePolicy,
        render_resource::PrimitiveTopology,
    },
};

#[derive(Component)]
pub struct Tile;

pub struct TileMesh {
    pub size: f32,
    pub border: f32,
}

impl TileMesh {
    pub fn new(border: f32) -> TileMesh {
        TileMesh { size: 1.0, border }
    }
}

impl From<TileMesh> for Mesh {
    fn from(tile: TileMesh) -> Mesh {
        let mut mesh = Mesh::new(
            PrimitiveTopology::TriangleList,
            RenderAssetPersistencePolicy::Keep,
        );

        mesh.insert_attribute(
            Mesh::ATTRIBUTE_POSITION,
            vec![
                Vec3::new(0.0, 0.0, 0.0),
                Vec3::new(tile.size, 0.0, 0.0),
                Vec3::new(0.0, 0.0, tile.border),
                Vec3::new(tile.size, 0.0, tile.border),
                Vec3::new(tile.size, 0.0, 0.0),
                Vec3::new(0.0, 0.0, tile.border),
                Vec3::new(0.0, 0.0, 0.0),
                Vec3::new(tile.border, 0.0, 0.0),
                Vec3::new(0.0, 0.0, tile.size),
                Vec3::new(tile.border, 0.0, tile.size),
                Vec3::new(tile.border, 0.0, 0.0),
                Vec3::new(0.0, 0.0, tile.size),
                Vec3::new(0.0, 0.0, 1.0),
                Vec3::new(tile.size, 0.0, 1.0),
                Vec3::new(0.0, 0.0, 1.0 - tile.border),
                Vec3::new(tile.size, 0.0, 1.0 - tile.border),
                Vec3::new(tile.size, 0.0, 1.0),
                Vec3::new(0.0, 0.0, 1.0 - tile.border),
                Vec3::new(1.0, 0.0, 0.0),
                Vec3::new(1.0 - tile.border, 0.0, 0.0),
                Vec3::new(1.0, 0.0, tile.size),
                Vec3::new(1.0 - tile.border, 0.0, tile.size),
                Vec3::new(1.0 - tile.border, 0.0, 0.0),
                Vec3::new(1.0, 0.0, tile.size),
            ],
        );
        mesh.set_indices(Some(Indices::U32(vec![
            0, 2, 1, 3, 4, 5, 6, 8, 7, 9, 10, 11, 12, 13, 14, 15, 17, 16, 18, 19, 20, 21, 23, 22,
        ])));
        mesh.duplicate_vertices();
        mesh.compute_flat_normals();

        mesh
    }
}

pub mod actions;
pub mod events;
mod hovered;
pub mod input;
pub mod reactions;
pub mod tiles;
pub mod ui;

use bevy::prelude::*;

use crate::{
    creature_info::Position,
    creatures::Creature,
    dying::Dead,
    graphics::{
        actions::{set_action_names, trigger_actions, update_action_buttons},
        events::{GraphicsDamage, GraphicsEvents, GraphicsMovement},
        hovered::{set_hovered, Hovered},
        input::{creature_validation, position_validation},
        reactions::{reaction_response, show_reaction_popup},
        tiles::{Tile, TileMesh},
        ui::{button_highlighting, end_turn, ui_setup},
    },
    initiative::Initiative,
    simulation::SimulationSet,
};

pub struct GraphicsPlugin;

impl Plugin for GraphicsPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(AmbientLight {
            color: Color::WHITE,
            brightness: 100.0,
        })
        .insert_resource(GraphicsEvents::default())
        .add_systems(Startup, (graphics_setup, ui_setup))
        .add_systems(
            Update,
            (
                (
                    add_creatures,
                    button_highlighting,
                    trigger_actions,
                    show_reaction_popup,
                    reaction_response,
                    set_action_names, // TODO: Turn start / Changed<Relations<Action>>
                    end_turn,
                    set_hovered,
                    GraphicsMovement::apply,
                    GraphicsDamage::apply,
                ),
                colour_creature,
                (position_validation, creature_validation),
            )
                .chain()
                .before(SimulationSet),
        )
        .add_systems(
            Update,
            update_action_buttons.run_if(resource_changed::<Initiative>),
        );
    }
}

fn graphics_setup(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    // Camera
    commands.spawn(Camera3dBundle {
        transform: Transform::from_xyz(10.0, 10.0, 0.0)
            .looking_at(Vec3::new(2.0, 0.0, 2.0), Vec3::Y),
        ..default()
    });

    // Tiles
    for x in -10..11 {
        for y in -10..11 {
            commands.spawn((
                PbrBundle {
                    mesh: meshes.add(TileMesh::new(0.025)),
                    material: materials.add(Color::WHITE),
                    transform: Transform::from_xyz(x as f32, 0.0, y as f32),
                    ..PbrBundle::default()
                },
                Tile,
                Position(x, y),
            ));
        }
    }
}

fn add_creatures(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    query: Query<(Entity, &Position), (With<Creature>, Without<Transform>)>,
) {
    for (creature, position) in &query {
        commands.entity(creature).insert(PbrBundle {
            mesh: meshes.add(shape::Cube::new(1.0)),
            material: materials.add(Color::WHITE),
            transform: position.as_transform(),
            ..PbrBundle::default()
        });
    }
}

fn colour_creature(
    initiative: Res<Initiative>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut query: Query<(Entity, &mut Handle<StandardMaterial>), (With<Creature>, Without<Hovered>)>,
    mut hovered: Query<&mut Handle<StandardMaterial>, With<Hovered>>,
    is_dead: Query<Has<Dead>, With<Creature>>,
) {
    for (creature, mut material) in &mut query {
        if is_dead.get(creature).unwrap() {
            *material = materials.add(Color::BLACK);
        } else if creature == *initiative.current() {
            *material = materials.add(Color::BLUE);
        } else {
            *material = materials.add(Color::WHITE);
        }
    }

    for mut material in &mut hovered {
        *material = materials.add(Color::YELLOW);
    }
}

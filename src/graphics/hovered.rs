use bevy::{
    ecs::system::SystemParam,
    prelude::{primitives::Plane3d, *},
    window::PrimaryWindow,
};

use crate::{creature_info::Position, creatures::Creature};

#[derive(SystemParam)]
pub struct HoveredEntity<'w, 's> {
    cameras: Query<'w, 's, (&'static Camera, &'static GlobalTransform)>,
    windows: Query<'w, 's, &'static Window, With<PrimaryWindow>>,
    positions: Query<'w, 's, (Entity, &'static Position, Has<Creature>)>,
}

impl HoveredEntity<'_, '_> {
    pub fn get_entity(&self) -> Option<Entity> {
        let (camera, cam_transform) = self.cameras.single();
        if let Some(cursor_position) = self.windows.single().cursor_position() {
            let ray = camera
                .viewport_to_world(cam_transform, cursor_position)
                .unwrap();
            let top_intersect = ray
                .get_point(ray.intersect_plane(Vec3::new(0.0, 1.0, 0.0), Plane3d::new(Vec3::Y))?);
            let bottom_intersect = ray.get_point(
                ray.intersect_plane(Vec3::ZERO, Plane3d::new(Vec3::Y))
                    .unwrap(),
            );

            let mut tile = None;
            for (ent, position, is_creature) in &self.positions {
                if !is_creature {
                    if position.contains_vec(&bottom_intersect) {
                        tile = Some(ent); // Prioritize creatures over tiles
                    }
                } else if position.contains_vec(&top_intersect)
                    || position.contains_vec(&bottom_intersect)
                {
                    // TODO: Isn't perfect, is good enough for now
                    return Some(ent);
                }
            }
            tile
        } else {
            None
        }
    }
}

#[derive(Component)]
pub struct Hovered;

pub fn set_hovered(
    mut commands: Commands,
    mut materials: ResMut<Assets<StandardMaterial>>,
    hovered: HoveredEntity,
    mut current: Query<(Entity, &mut Handle<StandardMaterial>), With<Hovered>>,
    button_state: Query<&Interaction, With<Button>>,
) {
    // We shouldn't set anything hovered if we are actually hovering a button
    let button_hovered = !button_state.iter().all(|&i| i == Interaction::None);

    if let Some(entity) = hovered.get_entity() {
        let mut same = false;
        for (ent, mut material) in &mut current {
            if ent != entity || button_hovered {
                commands.entity(ent).remove::<Hovered>();
                *material = materials.add(Color::WHITE);
            } else {
                same = true;
            }
        }

        if !same && !button_hovered {
            commands.entity(entity).insert(Hovered);
        }
    }
}

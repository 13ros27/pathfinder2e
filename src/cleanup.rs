use bevy::prelude::*;

use crate::{
    queue::{Queued, ShortLived},
    simulation::{SimCleanup, SimReact, SimStart, Simulation, SimulationInput},
};

pub fn cleanup_short_lived(
    mut commands: Commands,
    query: Query<Entity, (With<ShortLived>, Without<Queued>)>,
) {
    for ent in query.iter() {
        commands.entity(ent).despawn();
    }
}

pub fn cleanup_sim_input(mut commands: Commands, mut input: ResMut<SimulationInput>) {
    if input.is_some() {
        commands.entity(*input.unwrap()).despawn();
        **input = None;
    }
}

pub struct CleanupPlugin;

impl Plugin for CleanupPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            Simulation,
            cleanup_sim_input.after(SimStart).before(SimReact),
        )
        .add_systems(SimCleanup, cleanup_short_lived);
    }
}

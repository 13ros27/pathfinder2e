use bevy::prelude::*;

use crate::weapons::prelude::*;

// TODO: Should also have the Trip trait
/// Melee, Reach, Trip, Versatile P
///
/// Damage: 1d8 Slashing, Hands: 2
#[derive(Bundle, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Horsechopper {
    pub damage: BaseDamage,
    pub melee: Melee,
    pub reach: Reach,
    pub versatile: Versatile,
    pub hands: Hands,
}

impl Default for Horsechopper {
    fn default() -> Self {
        Self {
            damage: BaseDamage(Damage::new(D8, DamageType::Slashing)),
            melee: Melee,
            reach: Reach,
            versatile: Versatile::new(DamageType::Piercing),
            hands: Hands::Two,
        }
    }
}

impl Named for Horsechopper {
    const NAME: &'static str = "Horsechopper";
}

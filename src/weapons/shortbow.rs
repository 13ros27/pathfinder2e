use bevy::prelude::*;

use crate::weapons::prelude::*;

/// Ranged (increment 60, reload 0), Deadly D10
///
/// Damage: 1d6 Piercing, Hands: 2
#[derive(Bundle, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Shortbow {
    pub damage: BaseDamage,
    pub ranged: Ranged,
    pub deadly: Deadly,
    pub hands: Hands,
}

impl Default for Shortbow {
    fn default() -> Self {
        Self {
            damage: BaseDamage(Damage::new(D6, DamageType::Piercing)),
            ranged: Ranged {
                range_increment: 60,
                reload: 0,
            },
            deadly: Deadly(D10),
            hands: Hands::Two,
        }
    }
}

impl Named for Shortbow {
    const NAME: &'static str = "Shortbow";
}

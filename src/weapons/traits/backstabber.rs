use bevy::prelude::*;

use crate::{
    actions::{ActionResult, AttackDamage, Constant},
    conditions::Condition,
    creatures::Creature,
    flanking::FlatFooted,
    queue::Queued,
    relations::prelude::*,
    weapons::Item,
    Action,
};

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Backstabber;

impl Backstabber {
    pub fn apply(
        action_results: Query<
            (
                &Relation<AttackDamage>,
                &Relation<Action>,
                &Relation<Creature>,
            ),
            (With<ActionResult>, Without<Queued>),
        >,
        backstabber_weapons: RelationQuery<Item, Has<Backstabber>>,
        conditions: Query<&Relations<Condition>, With<Creature>>,
        flatfooted_conds: Query<(), (With<FlatFooted>, With<Condition>)>,
        mut attack_const: Query<&mut Constant, With<AttackDamage>>,
    ) {
        for (&attack_damage, &action, &target) in &action_results {
            // If the attack uses a weapon with backstabber and it targets a flat-footed creature
            if backstabber_weapons.get(*action).unwrap()
                && conditions
                    .get(*target)
                    .unwrap()
                    .iter()
                    .any(|&c| flatfooted_conds.contains(*c))
            {
                // Add one constant damage to the attack
                let mut constant = attack_const.get_mut(*attack_damage).unwrap();
                *constant = Constant(**constant + 1);
            }
        }
    }
}

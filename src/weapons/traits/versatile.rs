use bevy::prelude::*;

use crate::{
    actions::{AttackDamage, DoingAction},
    damage::DamageType,
    queue::Queued,
    relations::prelude::*,
    weapons::Item,
    Action,
};

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Versatile {
    damage_type: DamageType,
    enabled: bool,
}

impl Versatile {
    pub fn new(damage_type: DamageType) -> Self {
        Self {
            damage_type,
            enabled: true,
        }
    }
}

impl Versatile {
    pub fn apply(
        current_actions: Query<
            (&Relation<AttackDamage>, &Relation<Action>),
            (With<DoingAction>, Without<Queued>),
        >,
        versatile_weapons: RelationQuery<Item, &Versatile>,
        mut attack_type: Query<&mut DamageType, With<AttackDamage>>,
    ) {
        for (&attack_damage, &action) in &current_actions {
            // If the attack uses a weapon with the versatile trait and its enabled (aka using its alternate damage type)
            if let Ok(Versatile {
                damage_type,
                enabled: true,
            }) = versatile_weapons.get(*action)
            {
                // Change the damage type to the type specified by versatile
                let mut attack_type = attack_type.get_mut(*attack_damage).unwrap();
                *attack_type = *damage_type;
            }
        }
    }
}

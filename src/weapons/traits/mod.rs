mod backstabber;
mod deadly;
mod finesse;
mod versatile;

use bevy::prelude::*;

use crate::simulation::SimTweak;

pub use backstabber::Backstabber;
pub use deadly::Deadly;
pub use finesse::Finesse;
pub use versatile::Versatile;

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Agile;

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Reach;

pub struct WeaponTraitPlugin;

impl Plugin for WeaponTraitPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(
            SimTweak,
            (
                Backstabber::apply,
                Deadly::apply,
                Finesse::apply,
                Versatile::apply,
            ),
        );
    }
}

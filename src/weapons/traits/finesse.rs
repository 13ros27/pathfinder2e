use bevy::prelude::*;

use crate::{
    actions::{AttackBonus, DoingAction, StatValue},
    creature_info::StatsEnum,
    queue::Queued,
    relations::prelude::*,
    weapons::Item,
    Action,
};

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Finesse {
    pub use_dex: bool,
}

impl Default for Finesse {
    fn default() -> Self {
        Finesse { use_dex: true }
    }
}

impl Finesse {
    pub fn apply(
        current_actions: Query<
            (&Relation<Action>, &Relation<AttackBonus>),
            (With<DoingAction>, Without<Queued>),
        >,
        finesse_weapons: RelationQuery<Item, &Finesse>,
        mut stat_bonuses: Query<&mut StatValue, With<AttackBonus>>,
    ) {
        for (&action, attack_bonus) in &current_actions {
            // If the attack uses a weapon with finesse and it is set to use dexterity
            if let Ok(Finesse { use_dex: true }) = finesse_weapons.get(*action) {
                // Change the stat bonus to dexterity (probably from strength)
                let mut stat_bonus = stat_bonuses.get_mut(**attack_bonus).unwrap();
                stat_bonus.set_stat(StatsEnum::Dex);
            }
        }
    }
}

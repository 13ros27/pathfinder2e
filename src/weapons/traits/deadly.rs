use bevy::prelude::*;

use crate::{
    actions::{ActionResult, AttackDamage},
    dice::Dice,
    queue::Queued,
    relations::prelude::*,
    success::CritSuccess,
    weapons::Item,
    Action,
};

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Deadly(pub Dice);

impl Deadly {
    pub fn apply(
        action_results: Query<
            (&Relation<AttackDamage>, &Relation<Action>),
            (With<CritSuccess>, With<ActionResult>, Without<Queued>),
        >,
        deadly_weapons: RelationQuery<Item, &Deadly>,
        mut attack_dice: Query<&mut Dice, With<AttackDamage>>, // TODO: RelationQuery
    ) {
        for (&attack_damage, &action) in &action_results {
            // If the attack uses a weapon with the deadly trait
            if let Ok(Deadly(deadly_die)) = deadly_weapons.get(*action) {
                // Add the deadly die to the attack damage
                let mut dice_value = attack_dice.get_mut(*attack_damage).unwrap();
                *dice_value += *deadly_die;
            }
        }
    }
}

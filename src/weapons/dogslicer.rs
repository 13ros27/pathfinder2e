use bevy::prelude::*;

use crate::weapons::prelude::*;

/// Melee, Agile, Backstabber, Finesse
///
/// Damage: 1d6 Slashing, Hands: 1
#[derive(Bundle, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Dogslicer {
    pub damage: BaseDamage,
    pub melee: Melee,
    pub agile: Agile,
    pub backstabber: Backstabber,
    pub finesse: Finesse,
    pub hands: Hands,
}

impl Default for Dogslicer {
    fn default() -> Self {
        Self {
            damage: BaseDamage(Damage::new(D6, DamageType::Slashing)),
            melee: Melee,
            agile: Agile,
            backstabber: Backstabber,
            finesse: Finesse::default(),
            hands: Hands::One,
        }
    }
}

impl Named for Dogslicer {
    const NAME: &'static str = "Dogslicer";
}

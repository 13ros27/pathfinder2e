mod dogslicer;
mod horsechopper;
mod shortbow;
pub mod traits;

use bevy::prelude::*;

use crate::damage::Damage;

pub mod all {
    pub use super::{dogslicer::Dogslicer, horsechopper::Horsechopper, shortbow::Shortbow};
}

pub mod prelude {
    pub use super::{traits::*, BaseDamage, Item, Melee, Ranged};
    pub use crate::{
        damage::{Damage, DamageType},
        dice::*,
        held::{Hands, Innate},
        names::Named,
    };
}

pub use traits::WeaponTraitPlugin;

#[derive(Component, Clone, Copy, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BaseDamage(pub Damage);

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Melee;

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Ranged {
    pub range_increment: usize,
    pub reload: usize,
}

impl Ranged {
    /// Calculate the ranged increment penalty for a strike at this distance (-2 for each increment past the first)
    pub fn increment_penalty(&self, distance: usize) -> isize {
        -(distance as isize - 1).div_floor(self.range_increment as isize) * 2
    }
}

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Item;

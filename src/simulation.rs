use bevy::{
    ecs::{
        component::{ComponentInfo, TableStorage},
        schedule::ScheduleLabel,
        system::RunSystemOnce,
    },
    prelude::*,
};

use crate::{
    graphics::{actions::update_action_buttons, input::AwaitingInput},
    queue::ShortLived,
    relations::prelude::*,
};

#[derive(ScheduleLabel, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Simulation;
#[derive(
    ScheduleLabel, SystemSet, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash,
)]
pub struct SimStart;
#[derive(
    ScheduleLabel, SystemSet, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash,
)]
pub struct SimReact;
#[derive(
    ScheduleLabel, SystemSet, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash,
)]
pub struct SimTweak;
#[derive(
    ScheduleLabel, SystemSet, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash,
)]
pub struct SimFinish;
#[derive(
    ScheduleLabel, SystemSet, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash,
)]
pub struct SimCleanup;

#[derive(
    Resource, Clone, Copy, Debug, Default, Deref, DerefMut, PartialEq, Eq, PartialOrd, Ord, Hash,
)]
pub struct SimulationInput(Option<KindEntity<TriggerAction>>);

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TriggerAction;

impl Component for TriggerAction {
    type Storage = TableStorage;
    fn init_component_info(info: &mut ComponentInfo) {
        info.on_add(|mut w, e, _| {
            w.commands().entity(e).insert((AwaitingInput, ShortLived));
        });
    }
}

#[derive(Event, Clone, Copy, Debug, Deref, DerefMut, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SimulationEvent(KindEntity<TriggerAction>);

impl SimulationEvent {
    pub fn new(entity: Entity) -> Self {
        Self(KindEntity::<TriggerAction>::new(entity))
    }
}

#[derive(SystemSet, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SimulationSet;

fn run_simulation(world: &mut World) {
    let events = world
        .get_resource::<Events<SimulationEvent>>()
        .unwrap()
        .iter_current_update_events()
        .copied()
        .collect::<Vec<_>>();
    for event in events {
        let mut sim_input = world.get_resource_mut::<SimulationInput>().unwrap();
        **sim_input = Some(*event);
        let mut count = 0;
        loop {
            world.run_schedule(Simulation);

            count += 1;
            if count > 10 {
                bevy::utils::tracing::warn!("Reached 10 simulation steps");
            }

            if (world
                .query_filtered::<(), With<ShortLived>>()
                .iter(world)
                .is_empty()
                && world.get_resource::<SimulationInput>().unwrap().is_none())
                || !world
                    .query_filtered::<(), With<AwaitingInput>>()
                    .iter(world)
                    .is_empty()
            {
                if !world
                    .query_filtered::<(), With<AwaitingInput>>()
                    .iter(world)
                    .is_empty()
                {
                    bevy::utils::tracing::info!(
                        "Finished sim phase after {count} loops due to awaiting input"
                    );
                } else {
                    bevy::utils::tracing::info!(
                        "Finished sim phase after {count} loops due to lack of work"
                    );
                }
                break;
            }
        }
    }

    // Update the action buttons when we finish simulating in-case our actions changed
    world.run_system_once(update_action_buttons);

    world
        .get_resource_mut::<Events<SimulationEvent>>()
        .unwrap()
        .clear();
}

pub struct SimulationPlugin;

impl Plugin for SimulationPlugin {
    fn build(&self, app: &mut App) {
        app.init_schedule(Simulation)
            .init_schedule(SimStart)
            .init_schedule(SimReact)
            .init_schedule(SimTweak)
            .init_schedule(SimFinish)
            .init_schedule(SimCleanup)
            .init_resource::<SimulationInput>()
            .add_event::<SimulationEvent>()
            .add_systems(
                Simulation,
                (
                    run_schedule::<SimStart>.in_set(SimStart),
                    run_schedule::<SimReact>.in_set(SimReact),
                    run_schedule::<SimTweak>.in_set(SimTweak),
                    run_schedule::<SimFinish>.in_set(SimFinish),
                    run_schedule::<SimCleanup>.in_set(SimCleanup),
                )
                    .chain(),
            )
            .add_systems(Update, run_simulation.in_set(SimulationSet));
    }
}

fn run_schedule<T: ScheduleLabel + Default>(world: &mut World) {
    world.run_schedule(T::default());
}

use bevy::{
    ecs::{
        query::{QueryData, QueryEntityError, QueryFilter, ROQueryItem},
        system::SystemParam,
    },
    prelude::*,
};

use crate::relations::Relation;

#[derive(SystemParam)]
pub struct RelationQuery<'w, 's, R, D, F = ()>
where
    R: Component + Default,
    D: QueryData + 'static,
    F: QueryFilter + 'static,
{
    rels: Query<'w, 's, &'static Relation<R>>,
    dests: Query<'w, 's, D, (F, With<R>)>,
}

impl<'w, 's, R, D, F> RelationQuery<'w, 's, R, D, F>
where
    R: Component + Default,
    D: QueryData,
    F: QueryFilter,
{
    pub fn get(&self, entity: Entity) -> Result<ROQueryItem<'_, D>, QueryEntityError> {
        let related = self.rels.get(entity)?;
        self.dests.get(**related)
    }

    pub fn get_mut(&mut self, entity: Entity) -> Result<D::Item<'_>, QueryEntityError> {
        let related = self.rels.get(entity)?;
        self.dests.get_mut(**related)
    }
}

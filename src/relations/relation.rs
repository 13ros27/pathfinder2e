use bevy::{
    ecs::{
        component::{ComponentInfo, TableStorage},
        system::{EntityCommand, EntityCommands},
    },
    prelude::*,
    utils::HashSet,
};
use std::{hash::Hash, ops::Deref};

use crate::relations::KindEntity;

#[derive(Component, Clone, Copy, Debug, PartialOrd, Ord)]
pub struct Relation<T: Component + Default>(KindEntity<T>);

impl<T: Component + Default> PartialEq for Relation<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}
impl<T: Component + Default> Eq for Relation<T> {}
impl<T: Component + Default> Hash for Relation<T> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.hash(state);
    }
}

impl<T: Component + Default> Relation<T> {
    pub fn new_unchecked(entity: Entity) -> Self {
        Self(KindEntity::new(entity))
    }

    #[allow(clippy::wrong_self_convention)] // Relation is copy
    pub fn as_weak(self) -> KindEntity<T> {
        self.0
    }
}

impl<T: Component + Default> Deref for Relation<T> {
    type Target = Entity;
    fn deref(&self) -> &Entity {
        &self.0
    }
}

#[ghost::phantom]
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Owned<T: Component + Default>;

impl<T: Component + Default> Component for Owned<T> {
    type Storage = TableStorage;
    fn init_component_info(info: &mut ComponentInfo) {
        info.on_remove(|mut world, entity, _| {
            let related_entity = **world.get::<Relation<T>>(entity).unwrap();
            // We only want to despawn if the other still exists
            if world.get_entity(related_entity).is_some() {
                world.commands().entity(related_entity).despawn();
            }
        });
    }
}

#[derive(Component, Clone, Debug, Deref)]
pub struct Relations<T: Component + Default>(HashSet<Relation<T>>);

impl<T: Component + Default> Relations<T> {
    pub fn empty() -> Self {
        Self(HashSet::new())
    }

    pub fn new_unchecked(entities: Vec<Entity>) -> Self {
        Self(
            entities
                .into_iter()
                .map(|e| Relation::new_unchecked(e))
                .collect(),
        )
    }

    pub fn add(&mut self, entity: Relation<T>) {
        self.0.insert(entity);
    }

    pub fn append_unchecked(&mut self, entities: Vec<Entity>) {
        self.0
            .extend(entities.into_iter().map(|e| Relation::new_unchecked(e)));
    }

    pub fn extend(&mut self, new: Relations<T>) {
        self.0.extend(new.0);
    }

    pub fn remove(&mut self, to_remove: &Relation<T>) {
        self.0.remove(to_remove);
    }
}

pub trait RelationCommands {
    fn own_relation<F, T>(&mut self, target: Relation<T>) -> &mut Self
    where
        F: Component + Default,
        T: Component + Default;

    fn insert_relation<T>(&mut self, target: Entity) -> &mut Self
    where
        T: Component + Default;

    fn insert_bidir_relation<F, T>(&mut self, target: Entity) -> &mut Self
    where
        F: Component + Default,
        T: Component + Default;

    fn insert_owned_bidir_relation<F, T>(&mut self, target: Entity) -> &mut Self
    where
        F: Component + Default,
        T: Component + Default;

    fn insert_relations<T>(&mut self, targets: Vec<Entity>) -> &mut Self
    where
        T: Component + Default;

    fn insert_bidir_relations<F, T>(&mut self, targets: Vec<Entity>) -> &mut Self
    where
        F: Component + Default,
        T: Component + Default;

    fn extend<T: Component + Default>(&mut self, added: Relations<T>) -> &mut Self;
}

impl RelationCommands for EntityCommands<'_, '_, '_> {
    /// Adds an owned relation F -> T
    fn own_relation<F, T>(&mut self, target: Relation<T>) -> &mut Self
    where
        F: Component + Default,
        T: Component + Default,
    {
        self.insert((F::default(), target, Owned::<T>))
    }

    /// Inserts a relation Self -> T
    fn insert_relation<T>(&mut self, target: Entity) -> &mut Self
    where
        T: Component + Default,
    {
        self.commands().entity(target).insert(T::default());
        self.insert(Relation::<T>::new_unchecked(target))
    }

    /// Inserts a bi-directional relation F <-> T
    fn insert_bidir_relation<F, T>(&mut self, target: Entity) -> &mut Self
    where
        F: Component + Default,
        T: Component + Default,
    {
        let id = self.id();
        self.commands()
            .entity(target)
            .insert(Relation::<F>::new_unchecked(id))
            .insert(T::default());
        self.insert(Relation::<T>::new_unchecked(target))
            .insert(F::default())
    }

    /// Inserts a bi-directional relation F <-> T where F owns T
    fn insert_owned_bidir_relation<F, T>(&mut self, target: Entity) -> &mut Self
    where
        F: Component + Default,
        T: Component + Default,
    {
        self.insert_bidir_relation::<F, T>(target)
            .insert(Owned::<T>)
    }

    /// Inserts a relation Self -> [T]
    fn insert_relations<T>(&mut self, targets: Vec<Entity>) -> &mut Self
    where
        T: Component + Default,
    {
        for target in &targets {
            self.commands().entity(*target).insert(T::default());
        }
        self.insert(Relations::<T>::new_unchecked(targets))
    }

    /// Inserts a bi-directional relation F <-> [T]
    // TODO: This should probably be owned F -> T
    fn insert_bidir_relations<F, T>(&mut self, targets: Vec<Entity>) -> &mut Self
    where
        F: Component + Default,
        T: Component + Default,
    {
        let id = self.id();
        for target in &targets {
            self.commands()
                .entity(*target)
                .insert(Relation::<F>::new_unchecked(id))
                .insert(T::default());
        }
        self.insert(Relations::<T>::new_unchecked(targets))
            .insert(F::default())
    }

    fn extend<T: Component + Default>(&mut self, added: Relations<T>) -> &mut Self {
        self.add(Extend(added))
    }
}

struct Extend<T: Component + Default>(Relations<T>);

impl<T: Component + Default> EntityCommand for Extend<T> {
    fn apply(self, id: Entity, world: &mut World) {
        if let Some(mut related) = world.get_mut::<Relations<T>>(id) {
            related.extend(self.0);
        } else {
            world.entity_mut(id).insert(self.0);
        }
    }
}

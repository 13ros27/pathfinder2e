use bevy::prelude::*;
use std::{hash::Hash, marker::PhantomData, ops::Deref};

#[derive(Clone, Debug, PartialOrd, Ord)]
pub struct KindEntity<T: Component + Default>(Entity, PhantomData<T>);

impl<T: Component + Default> PartialEq for KindEntity<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0 == other.0
    }
}
impl<T: Component + Default> Eq for KindEntity<T> {}
impl<T: Component + Default> Hash for KindEntity<T> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.hash(state);
    }
}

impl<T: Component + Default> KindEntity<T> {
    pub fn new(entity: Entity) -> Self {
        Self(entity, default())
    }
}

impl<T: Component + Default> Deref for KindEntity<T> {
    type Target = Entity;
    fn deref(&self) -> &Entity {
        &self.0
    }
}

impl<T: Component + Clone + Default> Copy for KindEntity<T> {}

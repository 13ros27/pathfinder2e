use bevy::{ecs::system::EntityCommands, prelude::*};
use std::{fmt::Debug, ops::DerefMut};

use crate::{creatures::Creature, names::Named, relations::prelude::*};

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Condition;

pub trait ValueCondition: Copy + Component + DerefMut<Target = usize> {
    const ONE: Self;
}

pub trait ConditionCommandsExt {
    fn increment_condition<T: Named + ValueCondition>(&mut self);
    fn add_condition_unchecked<B: Bundle + Debug>(&mut self, condition: B);
    fn add_condition<C: Component + Debug + PartialEq>(&mut self, condition: C);
    fn remove_condition<C: Component + Debug + PartialEq>(&mut self, condition: C);
}

impl ConditionCommandsExt for EntityCommands<'_, '_, '_> {
    /// Increment the given condition on this creature if they have it, otherwise add it at 1
    fn increment_condition<T: Named + ValueCondition>(&mut self) {
        self.add(|entity: EntityWorldMut| {
            let creature = entity.id();
            let conditions = entity
                .world()
                .get::<Relations<Condition>>(creature)
                .unwrap();

            // Find this condition and increment it if they already have it
            for &condition in conditions.iter() {
                if let Some(&value) = entity.world().get::<T>(*condition) {
                    **entity.into_world_mut().get_mut::<T>(*condition).unwrap() += 1;
                    bevy::utils::tracing::info!(
                        "Incremented the {} condition by 1 to {} on entity {:?}",
                        T::NAME,
                        *value + 1,
                        creature
                    );
                    return;
                }
            }

            // If they did not already have this condition then add it with a value of 1
            let world = entity.into_world_mut();
            let new_condition = world
                .spawn((
                    Condition,
                    T::ONE,
                    Relation::<Creature>::new_unchecked(creature),
                ))
                .id();
            world
                .get_mut::<Relations<Condition>>(creature)
                .unwrap()
                .add(Relation::new_unchecked(new_condition));
            bevy::utils::tracing::info!(
                "Added the {} condition to entity {:?} with value 1 when incrementing",
                T::NAME,
                creature
            );
        });
    }

    /// Adds the given condition to this creature, not checking if it already exists
    fn add_condition_unchecked<B: Bundle + Debug>(&mut self, condition: B) {
        self.add(|entity: EntityWorldMut| {
            let creature = entity.id();
            let world = entity.into_world_mut();

            bevy::utils::tracing::info!(
                "Added the condition {:?} to creature {:?} (unchecked)",
                condition,
                creature
            );
            let new_condition = world.spawn((Condition, condition)).id();
            world
                .get_mut::<Relations<Condition>>(creature)
                .unwrap()
                .add(Relation::new_unchecked(new_condition));
        });
    }

    /// Adds the given condition to this creature if it doesn't already have a condition with this component and the same value
    fn add_condition<C: Component + Debug + PartialEq>(&mut self, condition: C) {
        self.add(|entity: EntityWorldMut| {
            let creature = entity.id();
            let world = entity.into_world_mut();

            // We have to clone the conditions otherwise we own world twice
            let conditions = world
                .get::<Relations<Condition>>(creature)
                .unwrap()
                .iter()
                .cloned()
                .collect::<Vec<_>>();

            for cond in conditions {
                // Check if cond is the same as the one we are adding
                if world
                    .query_filtered::<&C, With<Condition>>()
                    .get(world, *cond)
                    == Ok(&condition)
                {
                    return;
                }
            }

            // We didn't find the condition so we should add it
            bevy::utils::tracing::info!(
                "Added the condition {:?} to creature {:?}",
                condition,
                creature
            );
            let new_condition = world.spawn((Condition, condition)).id();
            world
                .get_mut::<Relations<Condition>>(creature)
                .unwrap()
                .add(Relation::new_unchecked(new_condition));
        });
    }

    /// Removes the given condition from this creature if it exists
    fn remove_condition<C: Component + Debug + PartialEq>(&mut self, condition: C) {
        self.add(move |entity: EntityWorldMut| {
            let creature = entity.id();
            let world = entity.into_world_mut();

            // We have to clone the conditions otherwise we own world twice
            let conditions = world
                .get::<Relations<Condition>>(creature)
                .unwrap()
                .iter()
                .cloned()
                .collect::<Vec<_>>();

            for cond in conditions {
                // Check if cond is the same as the one we are trying to remove
                if world
                    .query_filtered::<&C, With<Condition>>()
                    .get(world, *cond)
                    == Ok(&condition)
                {
                    // Despawn the condition entity and remove it from Relations<Condition>
                    world.despawn(*cond);
                    world
                        .get_mut::<Relations<Condition>>(creature)
                        .unwrap()
                        .remove(&cond);
                    bevy::utils::tracing::info!(
                        "Removed the condition {:?} from creature {:?}",
                        condition,
                        creature
                    );
                    break;
                }
            }
        });
    }
}

use bevy::{prelude::*, utils::HashSet};

use crate::{
    conditions::{Condition, ConditionCommandsExt},
    creature_info::Position,
    relations::prelude::*,
    weapons::{traits::Reach, Item, Melee},
    Action, Creature,
};

#[derive(Component, Clone, Copy, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct InReachOf(KindEntity<Creature>);

impl InReachOf {
    pub fn recalculate(
        mut commands: Commands,
        query: Query<Entity, (With<Creature>, Changed<Position>)>,
        creatures: Query<
            (
                &Relations<Condition>,
                Option<&Relations<Action>>,
                &Position,
                Entity,
            ),
            With<Creature>,
        >,
        reach_weapons: RelationQuery<Item, Has<Reach>, With<Melee>>,
        reach_conditions: Query<&InReachOf, With<Condition>>,
    ) {
        // Recalculated InReachOf for all creatures that have changed their position (and all creatures in their reach)
        for entity in &query {
            let mut removed_conditions = HashSet::new();

            let entity_pos = creatures.get(entity).unwrap().2;

            // Loop over all the conditions on the moved entity
            let (conditions, actions, _, _) = creatures.get(entity).unwrap();
            for &cond in conditions.iter() {
                // If this is a reach condition add it to removed conditions (may be removed from here in the next loop)
                if let Ok(InReachOf(creature)) = reach_conditions.get(*cond) {
                    removed_conditions.insert(**creature);
                }
            }

            // Find the longest reach in the moving creatures actions
            let mut entity_reach = 0;
            if let Some(actions) = actions {
                for &action in actions.iter() {
                    // If this action is a melee item then we check its reach
                    if let Ok(reach) = reach_weapons.get(*action) {
                        // TODO: There are things other than reach weapons that change this
                        let reach_distance = if reach { 10 } else { 5 };

                        if reach_distance > entity_reach {
                            entity_reach = reach_distance;
                        }
                    }
                }
            }

            // Check each creature and see if entity is within reach of them, if so we remove
            // this condition from removed_conditions and if it wasn't there we add a new condition.
            for (_, actions, position, ent) in &creatures {
                if ent != entity {
                    let distance = position.distance_reach(entity_pos);

                    // If this creature is within reach of the moved entity we should add InReachOf
                    if distance <= entity_reach {
                        commands
                            .entity(ent)
                            .add_condition(InReachOf(KindEntity::new(entity)));
                    } else {
                        commands
                            .entity(ent)
                            .remove_condition(InReachOf(KindEntity::new(entity)));
                    }

                    if let Some(actions) = actions {
                        for &action in actions.iter() {
                            // If this action is a melee item then we check its reach
                            if let Ok(reach) = reach_weapons.get(*action) {
                                // TODO: There are things other than reach weapons that change this
                                let reach_distance = if reach { 10 } else { 5 };

                                if distance <= reach_distance {
                                    if !removed_conditions.remove(&ent) {
                                        // This should be able to use add_condition_unchecked but this gets called with all creatures when we first simulate
                                        commands
                                            .entity(entity)
                                            .add_condition(InReachOf(KindEntity::new(ent)));
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
            }

            removed_conditions.iter().for_each(|ent| {
                commands
                    .entity(entity)
                    .remove_condition(InReachOf(KindEntity::new(*ent)));
            });
        }

        // TODO: This also needs to recalculate when strike actions change (in case this changed their reach)
    }
}

#[derive(Component, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct FlatFooted;

impl FlatFooted {
    pub fn recalculate() {
        todo!()
    }
}

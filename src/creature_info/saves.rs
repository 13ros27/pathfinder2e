use bevy::prelude::*;

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Fort(isize);

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Refl(isize);

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Will(isize);

#[derive(Bundle, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Saves {
    pub fort: Fort,
    pub refl: Refl,
    pub will: Will,
}

impl Saves {
    pub fn new(fort: isize, refl: isize, will: isize) -> Self {
        Saves {
            fort: Fort(fort),
            refl: Refl(refl),
            will: Will(will),
        }
    }
}

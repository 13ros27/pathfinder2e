use bevy::prelude::*;
use std::{
    cmp::{max, min},
    fmt::Display,
};

#[derive(Component, Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Position(pub isize, pub isize); // TODO: Will need changing in the future (grid position in x and y)

impl Position {
    fn diagonal_distance(&self, other: &Position) -> usize {
        isize::min((self.0 - other.0).abs(), (self.1 - other.1).abs()) as usize
    }

    fn straight_distance(&self, other: &Position) -> usize {
        isize::max((self.0 - other.0).abs(), (self.1 - other.1).abs()) as usize
    }

    pub fn distance(&self, other: &Position) -> usize {
        let diagonal = self.diagonal_distance(other);
        let straight = self.straight_distance(other);
        (diagonal.div_floor(2) + straight) * 5
    }

    pub fn distance_reach(&self, other: &Position) -> usize {
        let diagonal = self.diagonal_distance(other);
        let straight = self.straight_distance(other) - diagonal;
        if straight == 0 && diagonal == 2 {
            10 // 10 foot reach can reach two diagonals
        } else {
            (diagonal.div_floor(2) + diagonal + straight) * 5
        }
    }

    pub fn route(&self, other: &Position) -> Vec<Position> {
        let mut route = Vec::with_capacity(self.distance(other) + 1);
        route.push(*self);

        let x_direction = (other.0 - self.0).clamp(-1, 1);
        let y_direction = (other.1 - self.1).clamp(-1, 1);

        let diagonal = self.diagonal_distance(other) as isize;
        for i in 0..diagonal {
            route.push(Position(
                self.0 + (i + 1) * x_direction,
                self.1 + (i + 1) * y_direction,
            ));
        }
        for i in 0..(self.straight_distance(other) as isize - diagonal) {
            if (self.0 - other.0).abs() > (self.1 - other.1).abs() {
                route.push(Position(
                    self.0 + (diagonal + i + 1) * x_direction,
                    self.1 + diagonal * y_direction,
                ));
            } else {
                route.push(Position(
                    self.0 + diagonal * x_direction,
                    self.1 + (diagonal + i + 1) * y_direction,
                ));
            }
        }

        route
    }

    pub fn adjacent(&self, other: &Position) -> bool {
        (self.0 - other.0).abs() <= 1 && (self.1 - other.1).abs() <= 1
    }

    pub fn as_transform(&self) -> Transform {
        Transform::from_xyz(self.0 as f32 + 0.5, 0.5, self.1 as f32 + 0.5)
    }

    pub fn contains_vec(&self, vec: &Vec3) -> bool {
        vec.x >= self.0 as f32
            && vec.x < self.0 as f32 + 1.0
            && vec.z >= self.1 as f32
            && vec.z < self.1 as f32 + 1.0
    }
}

impl Display for Position {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.0, self.1)
    }
}

use bevy::{ecs::query::QueryData, prelude::*};
use std::fmt::Display;

#[derive(Component, Clone, Copy, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Str(isize);

#[derive(Component, Clone, Copy, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Dex(isize);

#[derive(Component, Clone, Copy, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Con(isize);

#[derive(Component, Clone, Copy, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Int(isize);

#[derive(Component, Clone, Copy, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Wis(isize);

#[derive(Component, Clone, Copy, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Cha(isize);

#[derive(Bundle, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct StatsBundle {
    pub str: Str,
    pub dex: Dex,
    pub con: Con,
    pub int: Int,
    pub wis: Wis,
    pub cha: Cha,
}

#[derive(QueryData)]
pub struct Stats {
    pub str: &'static Str,
    pub dex: &'static Dex,
    pub con: &'static Con,
    pub int: &'static Int,
    pub wis: &'static Wis,
    pub cha: &'static Cha,
}

impl StatsBundle {
    pub fn new(str: isize, dex: isize, con: isize, int: isize, wis: isize, cha: isize) -> Self {
        StatsBundle {
            str: Str(str),
            dex: Dex(dex),
            con: Con(con),
            int: Int(int),
            wis: Wis(wis),
            cha: Cha(cha),
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum StatsEnum {
    Str,
    Dex,
    Con,
    Int,
    Wis,
    Cha,
}

impl Display for StatsEnum {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                StatsEnum::Str => "Str",
                StatsEnum::Dex => "Dex",
                StatsEnum::Con => "Con",
                StatsEnum::Int => "Int",
                StatsEnum::Wis => "Wis",
                StatsEnum::Cha => "Cha",
            }
        )
    }
}

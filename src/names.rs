use bevy::prelude::*;

#[derive(Component, Clone, Debug, Deref, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct FixedName(pub String);

pub trait Named {
    const NAME: &'static str;
    fn get_name() -> FixedName {
        FixedName(Self::NAME.to_string())
    }
}

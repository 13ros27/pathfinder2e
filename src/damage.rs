use bevy::{
    ecs::component::{ComponentInfo, TableStorage},
    prelude::*,
};
use std::fmt::Display;

use crate::{
    actions::{AttackDamage, Constant},
    conditions::ConditionCommandsExt,
    creatures::Creature,
    dice::Dice,
    dying::{Dead, DeathThreshold, Dying},
    graphics::events::GraphicsEvents,
    initiative::Initiative,
    queue::{Queued, ShortLived},
    relations::prelude::*,
    Hp, MaxHP,
};

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Damage {
    pub dice: Dice,
    pub damage_type: DamageType,
}

impl Damage {
    pub fn new(dice: Dice, damage_type: DamageType) -> Self {
        Self { dice, damage_type }
    }
}

impl Display for Damage {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {}", self.dice, self.damage_type)
    }
}

#[derive(Component, Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum DamageType {
    Slashing,
    Piercing,
    Bludgeoning,
}

impl Display for DamageType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                DamageType::Slashing => "slashing",
                DamageType::Piercing => "piercing",
                DamageType::Bludgeoning => "bludgeoning",
            }
        )
    }
}

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DealDamage;

impl Component for DealDamage {
    type Storage = TableStorage;
    fn init_component_info(info: &mut ComponentInfo) {
        info.on_add(|mut w, e, _| {
            w.commands().entity(e).insert(ShortLived);
        });
    }
}

impl DealDamage {
    pub fn apply(
        mut commands: Commands,
        mut initiative: ResMut<Initiative>,
        query: Query<
            (&Relation<Creature>, &Relation<AttackDamage>),
            (With<DealDamage>, Without<Queued>),
        >,
        damages: Query<&Constant, With<AttackDamage>>,
        mut creatures: Query<(&mut Hp, &MaxHP, &DeathThreshold), With<Creature>>,
        mut graphics_events: ResMut<GraphicsEvents>,
    ) {
        for (&target, &damage) in &query {
            let damage_amount = **damages.get(*damage).unwrap();
            let (mut hp, &max_hp, &death_threshold) = creatures.get_mut(*target).unwrap();
            if damage_amount > 0 {
                graphics_events.send_damage(&mut commands, damage_amount as usize, target);
                if **hp > damage_amount as usize {
                    *hp = Hp(**hp - damage_amount as usize)
                } else {
                    *hp = Hp(0);

                    // If the creature has a death threshold and they don't take massive damage don't outright kill them
                    if *death_threshold > 0 && *max_hp * 2 > damage_amount as usize {
                        initiative
                            .move_to_end(target.as_weak())
                            .expect("Damage target should exist");
                        commands.entity(*target).increment_condition::<Dying>();
                    } else {
                        initiative
                            .remove(target.as_weak())
                            .expect("Damage target should exist");
                        commands.entity(*target).insert(Dead);
                    }
                }
            }
        }
    }
}
